<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/font-awesome.css" rel="apple-touch-icon-precomposed">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<meta name="description" content="<?php bloginfo('description'); ?>">
		<script src="http://code.jquery.com/jquery-2.1.3.js"></script>
  		<script src="<?php echo get_template_directory_uri(); ?>/js/hamburgler.js"> </script>
 		 <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/hamburgler.css">
		 

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>
        
        <!-- Google Analytics Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-77020351-1', 'auto');
  ga('send', 'pageview');

</script>

	</head>
	<body <?php body_class(); ?>

		<!-- wrapper -->
		<div class="site_wrapper">

			<!-- header -->
			<header>
				<div class="top_notification">
					<h4><img class="icon_nav" src="<?php echo get_template_directory_uri(); ?>/img/plane_icon.png">

						<?php
								if(is_active_sidebar('promo-banner')){
									dynamic_sidebar('promo-banner');
								}
							?>

						<img class="icon_nav heart_icon" src="<?php echo get_template_directory_uri(); ?>/img/heart_icon.png"></h4>	
				</div>

				<div class="top_bar">
					<div class="top_logo">
						<a href="/"><div class="tropic_logo"></div></a>
					</div>
				

					<!-- nav -->
					<div class="nav" role="navigation">
						<ul class="nav_links">
							<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
						</ul>

						<div class="nav_right">
							<ul>
								<li>
									<img class="icon_nav" src="<?php echo get_template_directory_uri(); ?>/img/user_icon.png">
									<a href="/my-account/">Your Account</a>
								</li>
								<li>
									<img class="icon_nav" src="<?php echo get_template_directory_uri(); ?>/img/bag_icon.png">
									<a href="/cart">Beach Bag</a>
								</li>
							</ul>
						</div>

					</div>
					<!-- /nav -->


					<!-- MOBILE MENU -->
					  <div class="mobilenav">
    <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
  </div>
  
  <a href="javascript:void(0)" class="icon">
    <div class="hamburger">
    <div class="menui top-menu"></div>
    <div class="menui mid-menu"></div>
    <div class="menui bottom-menu"></div>
    </div>
  </a>

				</div> <!-- .top_bar -->
			</header>
			<!-- /header -->

			<div class="page_wrapper">
				<div class="top_border">
					<img src="<?php echo get_template_directory_uri(); ?>/img/top_border.png">
				</div>
			
