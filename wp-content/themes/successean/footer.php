	</div>	
			<!-- footer -->
			<footer class="site_footer" role="contentinfo">
				<div class="top_footer">

					<div class="footer_left">
						<div class="footer_wrapper">
							<h4>Join the world of <span class="gold-font"> Hawaiian Tropic </span> Apparel and get 15% off your first order</h4>

							<div class="newsletter_form">
								<?php echo do_shortcode('[mc4wp_form id="900"]'); ?>
							</div>	
						</div>
					</div>

					<div class="footer_right">
						<div class="footer_wrapper">
							<div class="social_footer">
								<ul>
									<li><a href="https://www.facebook.com/hawaiiantropic" target="blank"><img class="social_icon" src="<?php echo get_template_directory_uri(); ?>/img/fb_icon.png"></a>
									</li>
									<!-- <li><a href="http://hawaiiantropicapparel.tumblr.com/" target="blank"><img class="social_icon" src="<//?php echo get_template_directory_uri(); ?>/img/twitter_icon2.png"></a>
									</li> -->
									<li><a href="https://www.instagram.com/hawaiiantropic/" target="blank"><img class="social_icon" src="<?php echo get_template_directory_uri(); ?>/img/ig_icon.png"></a>
									</li>
								</ul>
							</div>
						</div>
					</div>

				</div> <!-- .top_footer -->

				<div class="bottom_footer">
					<div class="footer_wrapper">

						<div class="footer_cols footer_col_1">
							<div class="foote_logo_img" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/tropic_logo_wht.png')"></div>
							<p class="copyright_p">© 2016Edgewell. Hawaiian Tropic is a trademark of Edgewell Personal Care Brands, LLC and is used under license by Como Fred David.</p>
						</div>

						<div class="footer_cols footer_col_2">
							<h3>The Brand</h3>
							<!-- Brand Menu 1 -->
							<ul>
							<?php
								if(is_active_sidebar('brand-widget-1')){
									dynamic_sidebar('brand-widget-1');
								}
							?>
							</ul>
							<!-- Brand Menu 2 -->
							<ul>
							<?php
								if(is_active_sidebar('brand-widget-2')){
									dynamic_sidebar('brand-widget-2');
								}
							?>
							</ul>
						</div>

						<div class="footer_cols footer_col_3">
							<h3>Online Shopping</h3>
							<ul>
							<?php
								if(is_active_sidebar('shopping-widget-1')){
									dynamic_sidebar('shopping-widget-1');
								}
							?>
							</ul>
							<ul>
							<?php
								if(is_active_sidebar('shopping-widget-2')){
									dynamic_sidebar('shopping-widget-2');
								}
							?>
							</ul>
						</div>

					</div>
				</div>
				
			</footer>
			<!-- /footer -->
<?php wp_footer(); ?>

	</body>

</html>
