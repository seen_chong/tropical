<?php get_header(); ?>

<div class="page_retailer_wrapper">

	<div class="page_retailer_header">
		<h1>Retailers</h1> 
	</div>

	<div class="retailer_location">
		<h4>United States</h4>

		<div class="retailer_stores">
			<h5>Independents / Boutiques</h5>
			

			<table style="width:100%">
			  <tr>
			  	<th>State</th>
			    <th>Name</th>
			    <th>Address</th> 
			    <th>Town/City</th>
			    <th>Code</th>
			  </tr>

		<?php
		  $args = array(
		    'post_type' => 'us-retailers',
		    'posts_per_page' => 99 
		    );
		  $products = new WP_Query( $args );
		  if( $products->have_posts() ) {
		    while( $products->have_posts() ) {
		      $products->the_post();
		?>
			  <tr>
			  	<td><?php the_field('state'); ?></td>
			    <td><?php the_field('retailer'); ?></td>
			    <td><?php the_field('address'); ?></td> 
			    <td><?php the_field('city'); ?></td>
			    <td><?php the_field('code'); ?></td>
			  </tr>

		<?php
	    		}
	  		}
		  else {
		    echo 'No Locations Found';
		  }
	  	?>

			</table>
		</div>

	</div>

		

</div>

</div> <!-- .page_wrapper -->
</div> <!-- .site_wrapper -->


<?php get_footer(); ?>

<script type="text/javascript">
jQuery(".retailer_stores h5").click(function(){

    jQuery("table").slideToggle("easeOutBounce");
});
</script>

