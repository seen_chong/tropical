<?php get_header(); ?>

<div class="page_wrapper">

		<div class="page_returns_header">
			<h3>Shipping Information</h3> 
		</div>

		<div class="media shipping_returns_hero shipping_info_hero"></div>
		<section class = "page_section returns_section">
			<div class="media shipping_returns_hero"></div>
			<!--YOU CAN JUST REMOVE EVERYTHING INSIDE OF THE P and inser the text entered in the admin-->
			<p>
				<article id="post-50" class="post-50 page type-page status-publish hentry">

									<p><span style="font-weight: 400;">SHIPPING &amp; HANDLING</span></p>
					<p><span style="font-weight: 400;">Orders placed during business days before 12:00PM PST will be processed and shipped same day. Any order placed after 12:00PM &nbsp;PST during business days will be processed by next business day.</span></p>
					<p><span style="font-weight: 400;">All orders are shipped via United States Postal Service (USPS), on business days generally between 9:00AM PST and 5:00PM PST (weekends and holidays are excluded). Shipments are fully traceable on our website and directly at </span><a href="http://www.usps.com"><span style="font-weight: 400;">www.usps.com</span></a><span style="font-weight: 400;">. Shipping and handling costs are updated in the shopping cart when customer inputs their shipping address.</span></p>
					<p><span style="font-weight: 400;">The date you receive your order will vary depending on the shipping method that you have selected at the time of the purchase. Please allow an additional 2-3 business days for deliveries to rural areas.</span></p>
					<p><span style="font-weight: 400;">At this time, we do not ship outside the U.S.A.</span></p>
					<p><span style="font-weight: 400;">OUT OF STOCK ITEMS</span></p>
					<p>
						<span style="font-weight: 400;">In most cases, when an item displayed on our website is no longer available, either the item will not appear on the site at all, or a message indicating the item is “Out of Stock” will appear on the product detail page. In some cases, however, due to inventory fluctuation, items that appear to be available when you place your order may, in fact, be out of stock. If this happens, we will notify you as quickly as possible that your order cannot be fulfilled. That item will be cancelled, and you will not be charged for that item.</span>
					</p>


				
				</article>
			</p>

		</section>
	</div>


</div>

<?php get_footer(); ?>