<?php get_header(); ?>


		<?php
		  $args = array(
		    'post_type' => 'stories'
		    );
		  $products = new WP_Query( $args );
		  if( $products->have_posts() ) {
		    while( $products->have_posts() ) {
		      $products->the_post();
		?>

		<div class="story_top_layer" style="background-image:url(<?php the_field('background_image'); ?>);">

			<div class="story_top_layer_overlay">
				<!-- <h1><span class="script">S</span>cent of the Sea</h1> -->
				<h1><?php the_field('collection_title'); ?></h1>
				<h5><?php the_field('banner_text'); ?></h5>
				<button><a href="/shop">Shop Collection</a></button>
			</div>

	</div>


	<div class="story_images">
		<div class="story_wrapper">

			<div class="story_image_left img_story">
				<figure>
  					<img class="story_img" src="<?php the_field('image_1'); ?>">	
  					<img class="marker"src="<?php echo get_template_directory_uri(); ?>/img/arrow_up_marker.png">
  					<figcaption><?php the_field('product_name_1'); ?></figcaption>
				</figure>
			</div>

			<div class="story_image_center img_story">
				<figure>
					<figcaption class="hide_on_590"><?php the_field('product_name_2'); ?></figcaption>
					<img class="marker hide_on_590" src="<?php echo get_template_directory_uri(); ?>/img/arrow_down_marker.png">

	  				<img class="story_img" src="<?php the_field('image_2'); ?>">	

	  				<img class="marker show_on_590"src="<?php echo get_template_directory_uri(); ?>/img/arrow_up_marker.png">
	  				<figcaption class="show_on_590"><?php the_field('product_name_2'); ?></figcaption>
				</figure>
			</div>

			<div class="story_image_right img_story">
				<figure>
					<img class="story_img"src="<?php the_field('image_3'); ?>">	
					<img class="marker"src="<?php echo get_template_directory_uri(); ?>/img/arrow_up_marker.png">
					<figcaption><?php the_field('product_name_3'); ?></figcaption>
				</figure>
			</div>

		</div>

		<?php
	    		}
	  		}
		  else {
		    echo 'No Stories Found';
		  }
	  	?>
	
	</div> <!-- .story_images -->


	<div class="story_chapters">
		<div class="story_wrapper">
			<img class="arrow_left" src="<?php echo get_template_directory_uri(); ?>/img/arrow_left.png">
			<div class="prev_story prev_next_story">
				<h3>Previous Collection</h3>
				<figure>
					<img src="<?php echo get_template_directory_uri(); ?>/img/prev_img.jpg">	
					<figcaption><span class="script">D</span> own to Earth</figcaption>
				</figure>
					
			</div>

			<div class="next_story prev_next_story">
				<h3>Next Collection</h3>
				<figure>
					<img src="<?php echo get_template_directory_uri(); ?>/img/next_img.png">	
					<figcaption><span class="script">W</span> elcome to Miami</figcaption>
				</figure>
					
			</div>
			<img class="arrow_left" src="<?php echo get_template_directory_uri(); ?>/img/arrow_right.png">
		</div>
	</div>

	</div>

</div> <!-- .page_wrapper -->

<?php get_footer(); ?>
