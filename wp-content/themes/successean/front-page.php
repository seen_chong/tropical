<?php get_header(); ?>
	
	<div class="top_layer" style="background-image:url(<?php the_field('top_banner'); ?>);">
		<img src="<?php the_field('top_banner'); ?>" style="visibility:hidden"/>
		<div class="top_layer_overlay">
			<h1><span class="script">N</span>ew Arrivals</h1>
			<h3><?php the_field('top_banner_text'); ?></h3>
			<a href="<?php the_field('top_banner_link'); ?>"><button>Shop now</button></a>
		</div>	
	</div>

	<div class="mid_layer" style="background-image:url(<?php the_field('thin_banner_image'); ?>);">
		<img src="<?php the_field('thin_banner_image'); ?>"/>
		<div class="mid_layer_overlay">
			<ul>
				<li class="mid_text_2">
					<a class="btn" data-popup-open="popup-1" href="#">
						<?php the_field('thin_banner_text_2'); ?>
						<span class="fakeBtn">Subscribe</span>
					</a>
				</li>
			</ul>
		</div>
	</div>

<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
        <h4>Join the world of <span class="gold-font"> Hawaiian Tropic </span> Apparel and get 15% off your first order</h4>

		<div class="newsletter_form">
			<?php echo do_shortcode('[mc4wp_form id="900"]'); ?>
		</div>	
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>



			<div class="flexbox_shop">

				<div class="flex_vert flex_images rose">
					<a href="<?php the_field('banner_1_link'); ?>" id="blog_flex_link">
						<img src="<?php the_field('banner_1'); ?>"/>
						<div class="bg_img_media" style="background-image:url('<?php the_field('banner_1'); ?>')"></div>

						<div class="flex_vert_overlay">
							<h1><span class="script">S</span>hop Caftans
							</h1>
						</div>
					</a>
					<a href="<?php the_field('banner_4_link'); ?>" id="product_category_link_left">
						<img src="<?php the_field('banner_4'); ?>"/>
						<div class="bg_img_media" style="background-image:url('<?php the_field('banner_4'); ?>')"></div>
						<div class="flex_vert_overlay">
							<h1><span class="script">S </span>hop Beach Dresses
							</h1>
						</div>
					</a>
				</div>


				<div class="flex_stack flex_images">
					<a href="<?php the_field('banner_2_link'); ?>" id="product_category_link_right">
						<img src="<?php the_field('banner_2'); ?>" />
						<div class="bg_img_media" style="background-image:url('<?php the_field('banner_2'); ?>')"></div>
						<div class="flex_vert_overlay">
							<h1><span class="script">S </span>hop All Bottoms
							</h1>
						</div>
					</a>
					<a href="<?php the_field('banner_3_link'); ?>" id="collection_flex_link">
						<img src="<?php the_field('banner_3'); ?>" />
						<div class="bg_img_media" style="background-image:url('<?php the_field('banner_3'); ?>')"></div>
						<div class="flex_vert_overlay">
							<h1><span class="script">R </span>elax in The Sunrise to Sunset Collection
							</h1>
						</div>
					</a>					
				</div>			

			  <!-- images placed inside block elements to deal with a Firefox rendering bug affecting  scaled flexbox images -->

			</div> <!-- .flexbox_shop -->



	</div> <!-- .page_wrapper -->
</div> <!-- .site_wrapper -->

<?php get_footer(); ?>

<script type="text/javascript">
$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
 
        e.preventDefault();
    });
 
    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
 
        e.preventDefault();
    });
});
</script>
