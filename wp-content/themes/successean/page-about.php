<?php get_header(); ?>


	<div class="page_about_header">
		<h1><?php the_field('about_title'); ?></h1> 
		<p><?php the_field('about_intro'); ?></p>
	</div>

	<div class="page_about_container">

		<div class="page_about_wrapper">

			<div class="about_center_panel">
				<h2>Hawaiian Tropic Apparel</h2>
				<p><?php the_field('about_text'); ?></p>
				<a href="/shop">
					<button class="about_shop">Shop Now</button>
				</a>

				<div class="about_portions">
					<h4>Collections</h4>
					<p><?php the_field('collections_text'); ?></p>
					<a href="/stories/scent-of-the-sea/">
						<button>View</button>
					</a>
				</div>
				<div class="about_portions about_company">
					<h4>Company</h4>
					<p><?php the_field('company_text'); ?></p>
					<a href="/contact">
						<button>Contact</button>
					</a>
				</div>

				<img class="about_logo" src="<?php echo get_template_directory_uri(); ?>/img/tropic_logo_1.png">
			</div>

		</div>

	</div>

</div>
</div> <!-- .page_wrapper -->

<?php get_footer(); ?>
