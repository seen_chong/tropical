<?php get_header(); ?>
​


<div class="page_sizing_wrapper">

		<div class="fittingContentWrapper">
			<div class="fittingHeader">
				<div class="fittingHeaderMedia" style="background-image:url(<?php the_field('banner'); ?>)"></div>
				<div class="fittingHeaderCopyBox">
					<h1>Fitting and Size Guide</h1>
					<h6>Comfort and elegance - these are two most important ingredients for success in designing our line of apparel. When shopping for beachware it should be properly adjusted to your unique size and shape. Please use the fitting and size guide to help decide the right fit for you.</h6>
				</div>
			</div>
			<div class="fittingTable" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/sizingGuide.png')"></div>
			<div class="fittingAdditionalInfo">
				<h1>How to Measure</h1>
				<div class="infoBox">
					<b>Bust Measurement</b>
					<p>Measure across the fullest part of the bust by threading a soft measuring tape across your back at band level and under each arm, being careful that your elbows are down and that the tape isn't pulled away from the body in the front. With the tape in position, take a breath in and out, allowing it to slide to its most comfortable, non-binding fit.</p>
					<i><span>TIP:</span> If the measurement falls on the half inch, round up.</i>
				</div>
				<div class="infoBox">
					<b>Waist Measurement</b>
					<p>Bend to one side to find natural indentation in torso. This is your natural waist. Run tape around natural waistline, keeping tape parallel with floor and one finger between body and tape for a more comfortable.</p>
				</div>
				<div class="infoBox">
					<b>Hip Measurement</b>
					<p>Stand on a level surface with feet together. Measure around fullest part of hips and bottom (approximately 7"-9" below waitline), keeping tape parallel with floor.</p>
				</div>
				<div class="infoBox">
					<b>Thigh Measurement</b>
					<p>Stand with heels apart and measure around fullest part of thigh (approximately 1" below crotch), keeping tape parallel with floor.</p>
				</div>
			</div>
		</div>
</div>

</div> <!-- .page_wrapper -->
</div> <!-- .site_wrapper -->
<?php get_footer(); ?>


