<?php get_header(); ?>

<div class="retex_wrapper">

	<div class="page_retex_header">
		
			<h1>How can we help you today?</h1>
		<div class="retex_header_text">
			<h3>Shipping Information</h3> 
			<p>Here you'll find information relating to our Shipment Procedures and our Delivery Schedules.</p>
		</div>
		<div class="retex_header_button">
			<button class="expand_all">Expand All</button>
		</div>

	</div>

	<div class="retex_block">

		<?php
		  $args = array(
		    'post_type' => 'shipping-info',
		    'posts_per_page' => 99 
		    );
		  $products = new WP_Query( $args );
		  if( $products->have_posts() ) {
		    while( $products->have_posts() ) {
		      $products->the_post();
		?>	

			<div class="single_block">


				<div class="retex_question">
					<h5><?php the_field('topic'); ?></h5>
					<button class="expand_btn">Expand</button>
					<button class="collapse_btn">Collapse</button>
				</div>
				
				<div class="retex_answer">
					<p><?php the_field('policy'); ?></p>
					<div class="retex_contact">
						<p>Still can't find your answer?</p>
						<button><a onclick="window.open('/contact');">Contact Us</a></button>
					</div>
				</div>   
			</div>

		<?php
	    		}
	  		}
		  else {
		    echo 'No Content Found';
		  }
	  	?>


	</div>



		

</div>

</div> <!-- .page_wrapper -->
</div> <!-- .site_wrapper -->


<?php get_footer(); ?>

<script type="text/javascript">
jQuery("button.expand_all").click(function(){

    jQuery(".retex_answer").show("easeOutBounce");
    jQuery(".expand_btn").hide();
    jQuery(".collapse_btn").show();
});

jQuery(".single_block").click(function(e){

    jQuery(".retex_answer", this).slideToggle("easeOutBounce");
    jQuery(".expand_btn", this).toggle();
    jQuery(".collapse_btn", this).toggle();
     return false;
});
</script>

