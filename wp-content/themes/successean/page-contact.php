<?php get_header(); ?>

<div class="page_contact_wrapper">

	<div class="page_contact_header">
		<div class="contact_hero">
			<div class="rectangle"></div>
			<div class="square"></div>
		</div>
		<h1>Contact</h1> 
		<h4>Have a question about a product, your order, or buying from hawaiian tropic apparel?</h4>
		<p><?php the_field('text'); ?></p>
	</div>
		

	<div class="contact_page_form">
		<p id="contact_address">255 Decarie Blvd  &bull;  Montreal, QC &bull; H4N2L7 &bull; 5142862666 &bull; info@hawaiiantropicapparel.ca</p>
		<?php echo do_shortcode('[contact-form-7 id="4" title="Contact Form"]'); ?>
	</div>

</div>

</div> <!-- .page_wrapper -->
</div> <!-- .site_wrapper -->



<//?php get_sidebar('locations'); ?>

<?php get_footer(); ?>
