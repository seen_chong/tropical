<?php get_header(); ?>


		<div class="story_top_layer" style="background-image:url(<?php the_field('background_image'); ?>);">

			<div class="story_top_layer_overlay">
				<!-- <h1><span class="script">S</span>cent of the Sea</h1> -->
				<h1 style="display:none;" class="show_on_story_responsive"><?php the_field('collection_title'); ?></h1>
				<div style="position:absolute;font-family:'BrandonGrotesque-Light;'"> </div>
				<h1 style="font-family:'BrandonGrotesque-Light;'"><canvas id="canvas_story_name" width=690 height=88></canvas></h1>
				<div style="position:absolute;font-family:'BrandonGrotesque-Light;'"> </div>
				<script>
					setTimeout(function() {
						var canvas = document.getElementById("canvas_story_name");
				        var ctx = canvas.getContext("2d");
				        ctx.save();
				        ctx.beginPath();
				        ctx.fillStyle = "rgba(123, 138, 179, 1)";
				        ctx.rect(0, 0, canvas.width, canvas.height);
				        ctx.fill();
				        var fontsize = 78;
				        ctx.font = fontsize + "px BrandonGrotesque-Light";
				        ctx.globalCompositeOperation = "xor";
				        ctx.beginPath();
				        ctx.textAlign = 'center';
				        ctx.textBaseline = "middle";
				        var text = "<?php the_field('collection_title'); ?>".toUpperCase().split("").join(String.fromCharCode(8201));

				        var width = ctx.measureText("<?php the_field('collection_title'); ?>".toUpperCase().split("").join(String.fromCharCode(8201))).width
					    do {
					        fontsize--;
					        ctx.font = fontsize + "px BrandonGrotesque-Light";
					    } while (ctx.measureText(text).width > (canvas.width - 180))

						ctx.fillText(text,canvas.width/2,canvas.height/2);
				        ctx.restore();
				       },600);
			    </script>	
				<h5><?php the_field('banner_text'); ?></h5>
				<button><a href="/product-category/<?php the_field('page_link'); ?>">Shop Collection</a></button>
			</div>

	</div>


	<div class="story_images">
		<div class="story_wrapper">

			<div class="story_image_left img_story">
				<a href="<?php the_field('link_1'); ?>">
					<figure>
	  					<img class="story_img" src="<?php the_field('image_1'); ?>">	
	  					<img class="marker"src="<?php echo get_template_directory_uri(); ?>/img/arrow_up_marker.png">
	  					<figcaption><?php the_field('product_name_1'); ?></figcaption>
					</figure>
				</a>
			</div>

			<div class="story_image_center img_story">
				<a href="<?php the_field('link_2'); ?>">
					<figure>
						<figcaption class="hide_on_590"><?php the_field('product_name_2'); ?></figcaption>
						<img class="marker hide_on_590" src="<?php echo get_template_directory_uri(); ?>/img/arrow_down_marker.png">

		  				<img class="story_img" src="<?php the_field('image_2'); ?>">	

		  				<img class="marker show_on_590"src="<?php echo get_template_directory_uri(); ?>/img/arrow_up_marker.png">
		  				<figcaption class="show_on_590"><?php the_field('product_name_2'); ?></figcaption>
					</figure>
				</a>
			</div>

			<div class="story_image_right img_story">
				<a href="<?php the_field('link_3'); ?>">
					<figure>
						<img class="story_img"src="<?php the_field('image_3'); ?>">	
						<img class="marker"src="<?php echo get_template_directory_uri(); ?>/img/arrow_up_marker.png">
						<figcaption><?php the_field('product_name_3'); ?></figcaption>
					</figure>
				</a>
			</div>

		</div>

	
	</div> <!-- .story_images -->


	<div class="story_chapters">
		<div class="story_wrapper">
			<h2><i>All Collections</i></h2>
			<div class="prev_story prev_next_story">
				<a href="/stories/down-to-earth/">
				<figure>
					
					<!-- <img class="arrow_left" src="<?php echo get_template_directory_uri(); ?>/img/arrow_left.png"> -->
					<img class="circle_collect" src="<?php the_field('earth_button', 13); ?>">	
					
					<figcaption><span class="script">D</span> own to Earth</figcaption>
				</figure>
					</a>
			</div>

			<div class="prev_story prev_next_story middle_story">
				<a href="/stories/sunrise-to-sunset/">
				<figure>
					
					<!-- <img class="arrow_left" src="<?php echo get_template_directory_uri(); ?>/img/arrow_left.png"> -->
					<img class="circle_collect" src="<?php the_field('sunrise_button', 13); ?>">		
					
					<figcaption><span class="script">S</span> unrise to Sunset</figcaption>
				</figure>
					</a>
			</div>

			<div class="next_story prev_next_story">
				<a href="/stories/scent-of-the-sea">
				<figure>
					<img class="circle_collect" src="<?php the_field('sos_button', 13); ?>">		
					<!-- <img class="arrow_right" src="<?php echo get_template_directory_uri(); ?>/img/arrow_right.png"> -->
					<figcaption><span class="script">S</span> cent of the Sea</figcaption>
				</figure>
					</a>
			</div>

			<div class="next_story prev_next_story">
				<a href="/stories/welcome-to-miami">
				<figure>
					<img class="circle_collect" src="<?php the_field('miami_button', 13); ?>">	
					<!-- <img class="arrow_right" src="<?php echo get_template_directory_uri(); ?>/img/arrow_right.png"> -->
					<figcaption><span class="script">W</span> elcome to Miami</figcaption>
				</figure>
					</a>
			</div>

		</div>
	</div>

	</div>
	<div style="position:absolute;font-family:'BrandonGrotesque-Light;'"> </div>
</div> <!-- .page_wrapper -->

<?php get_footer(); ?>
