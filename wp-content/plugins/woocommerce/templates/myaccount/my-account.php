<?php
/**
 * My Account page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

	<div class="page_logged_container">

		<div class="page_about_wrapper">

			<div class="about_center_panel">
				<h2>My Account</h2>
				<p><?php the_field('about_text'); ?></p>

				<div class="about_portions">
					<h4>My Wishlist</h4>
					<a href="/wishlist/view">
						<button>View</button>
					</a>
				</div>
				<div class="about_portions about_company">
					<h4>			<?php
				printf( __( 'Edit Account', 'woocommerce' ),
					wc_customer_edit_account_url()
				);
			?></h4>
					<a href="/my-account/edit-account">
						<button>Edit</button>
					</a>
				</div>

				<div class="about_portions">
					<h4>Find A Store</h4>
					<a href="/store-locator">
						<button>Locate</button>
					</a>
				</div>
				<div class="about_portions about_company">
					<h4>Contact Us</h4>
					<a href="/contact">
						<button>Contact</button>
					</a>
				</div>

				<button class="about_shop">		<?php
				printf(
					__( '<a href="%2$s">Log out</a>', 'woocommerce' ) . ' ',
					$current_user->display_name,
					wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
				);
			?></button>

			</div>

		</div>

	</div>
<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>
</div>
</div> <!-- .page_wrapper -->
