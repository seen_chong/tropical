<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;

?>
<div class="images">
	<?php
		if ( has_post_thumbnail() ) {
			$image_caption = get_post( get_post_thumbnail_id() )->post_excerpt;
			$image_link    = wp_get_attachment_url( get_post_thumbnail_id() );
			$image         = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
				'title'	=> get_the_title( get_post_thumbnail_id() )
			) );

			$attachment_count = count( $product->get_gallery_attachment_ids() );

			if ( $attachment_count > 0 ) {
				$gallery = '[product-gallery]';
			} else {
				$gallery = '';
			}

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_caption, $image ), $post->ID );

		} else {

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

		}
	?>

	<//?php do_action( 'woocommerce_product_thumbnails' ); ?>

		<div class="main_prod_details">		
			
			<table class="prod_size_table" style="display:none;">
				<p>Model is 5'8 1/2 and is wearing a size S</p>
				<h4 id="h4_first">Notes & Details</h4>
				<h5 class="size_guide_trigger" style="cursor:pointer;">Size Guide</h5>
			  <tr>
			    <th style="width:20%"></th>
			    <th style="width:20%"></th> 
			    <th style="width:20%"></th>
			    <th style="width:20%"></th>
			    <th style="width:20%"></th>
			  </tr>

			  <tr class="size_row">
			    <td><strong>Size</strong></td>
			    <td><strong>S</strong></td> 
			    <td><strong>M</strong></td> 
			    <td><strong>L</strong></td> 
			    <td><strong>XL</strong></td> 
			  </tr>

			  <tr class="cup_size_row">
			    <td>Bust</td>
			    <td>34.5-36.5</td> 
			    <td>36.5-39</td> 
			    <td>39-41.5</td> 
			    <td>41.5-43.5</td> 
			  </tr>

			  <tr class="cup_size_row">
			    <td>Waist</td>
			    <td>27-29</td> 
			    <td>29-31.5</td> 
			    <td>31.5-34</td> 
			    <td>34-36</td> 
			  </tr>

			  <tr class="cup_size_row">
			    <td>Hip</td>
			    <td>37.5-39.5</td> 
			    <td>39.5-42</td> 
			    <td>42-44.5</td> 
			    <td>44.5-46.5</td> 
			  </tr>

			  <tr class="cup_size_row">
			    <td>Thigh</td>
			    <td>22-23.5</td> 
			    <td>23.5-25.25</td> 
			    <td>25.25-27.25</td> 
			    <td>27.25-29.25</td> 
			  </tr>
			</table>


			
			<table class="prod_size_table">
				<h4 class="ship_trigger" id ="h4_second" style="cursor:pointer;">Shipping & Returns</h4>
				<p class="ship_ret_text ship_reveal" style="display:none;">Place your order today and get it in 1-5 business days. Free returns on regular items. Promotional items are not eligible. $99 or more and we'll cover the shipping cost!</p>
				<h4 id ="h4_second" class="spec_trigger" style="cursor:pointer;">Product Specifications</h4>
				<p class="ship_ret_text spec_reveal" style="display:none;"><?php the_field('product_specs'); ?></p>
				<h4 id ="h4_third">Share This</h4>
				<ul class="social_icons">
					<?php echo do_shortcode( '[woocommerce_social_media_share_buttons]' ); ?>
				</ul>
			</table>
		</div>
					<?php
  global $product;
 $attachment_ids = $product->get_gallery_attachment_ids();

foreach( $attachment_ids as $attachment_id ) 
{
  //Get URL of Gallery Images - default wordpress image sizes
  // echo $Original_image_url = wp_get_attachment_url( $attachment_id );
  
  //Get URL of Gallery Images - WooCommerce specific image sizes
  // echo $shop_thumbnail_image_url = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' )[0];
  // echo $shop_catalog_image_url = wp_get_attachment_image_src( $attachment_id, 'shop_catalog' )[0];
  // echo $shop_single_image_url = wp_get_attachment_image_src( $attachment_id, 'shop_single' )[0];
  
  //echo Image instead of URL
  echo wp_get_attachment_image($attachment_id, 'full');
}
?>


</div>


 <script type="text/javascript">
jQuery(".size_guide_trigger").click(function(){
    jQuery(".prod_size_table").toggle();
});
jQuery(".ship_trigger").click(function(){
    jQuery(".ship_reveal").toggle();
});
jQuery(".spec_trigger").click(function(){
    jQuery(".spec_reveal").toggle();
});
</script>

