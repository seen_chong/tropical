=== WooCommerce Netbanx Gateway ===

This plugin allows you to use Netbanx payment gateway within your WooCommerce store.

== Description ==

A plugin that allows you to extend your WooCommerce installation with the Netbanx payment processor

This plugin was created by: vanbo.develops(Ivan Andreev)

== Installation ==

1. Upload the `woocommerce-gateway-netbanx` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Navigate to `WooCommerce > Settings > Payment Gateways` to configure the Netbanx gateway settings.


Steps for configuration of your Netbanx installation.
	1. Login to your WooCommerce store
	2. Enable/Disable Netbanx.
	3. Set your "Method Title" and "Description". These options are seen on the checkout page.
	4. Enter your "Shop ID".
	5. Enter Netbanx "Shop Key".
    6. Enter your "Merchant Account Number", if needed.
	7. Enable "Debug Log", if you want to get a log of the request and response steps and parameters.
    8. Save Changes.
    9. Recommended: make a few test payments before you go live. In live mode make a couple small live test payments, too.

== Options ==

Enable/Disable:
		Enable Netbanx.

Sandbox:
		The option will turn on the sandbox feature of the gateway and enable you to make test payments.

Method Title: 
		This controls the title which the user sees during checkout.

Description: 
		This controls the description which the user sees during checkout.
		
Shop ID:
		Once your account has been set up, NETBANX will provide you with Shop ID, used as identification in requests sent to NETBANX.
							 
Shop Key: 
		Once your account has been set up, NETBANX will provide you with Shop Key, used to digitally sign requests sent to NETBANX.

Merchant Account Number:
		This is the merchant account number. If you have more than one merchant account with the same currency, you must provide the Account Number, if not leave empty.

Debug Log: 
		Recommended: Test Mode only
		Debug log will provide you with most of the data and events generated by the payment process. Logged inside woocommerce/logs/netbanx.txt
		