<?php
/**
 * Iframe HTML template.
 *
 * Don't edit this template directly as it will be overwritten with every plugin update.
 * Override this template by copying it to yourtheme/woocommerce/netbanx/iframe.php
 *
 * @since 2.0
 * @author VanboDevelops
 */
?>
<iframe
	name="netbanx-iframe"
	id="netbanx-iframe"
	src="<?php echo $location; ?>"
	frameborder="0"
	width="<?php echo $width; ?>"
	height="<?php echo $height; ?>"
	scrolling="<?php echo $scroll; ?>"
	>
	<p>
		<?php echo sprintf(
			__(
				'Your browser does not support iframes.
			 %sClick Here%s to get redirected to Netbanx payment page. ', 'wc_netbanx'
			),
			'<a href="' . $location . '">',
			'</a>'
		); ?>
	</p>
</iframe>