<?php
/*
 * Plugin Name: WooCommerce Netbanx Payment Gateway
 * Plugin URI: http://www.vanbodevelops.com/product/netbanx-payment-gateway-for-woocommerce
 * Description: Allows you to use <a href="http://www1.netbanx.com">Netbanx</a> payment processor with the WooCommerce plugin.
 * Version: 2.0.2
 * Author: VanboDevelops
 * Author URI: http://www.vanbodevelops.com
 *
 *	Copyright: (c) 2012 - 2015 VanboDevelops
 *	License: GNU General Public License v3.0
 *	License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

/**
 * Check if WooCommerce is active
 **/
if ( ! function_exists( 'is_vanbo_wc_active' ) ) {
	function is_vanbo_wc_active() {
		$active_plugins = (array) get_option( 'active_plugins', array() );

		if ( is_multisite() ) {
			$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
		}

		if ( in_array( 'woocommerce/woocommerce.php', $active_plugins ) || array_key_exists( 'woocommerce/woocommerce.php', $active_plugins ) ) {
			return true;
		}

		return false;
	}
}

if ( ! is_vanbo_wc_active() ) { return; }

class WC_Netbanx {

	/**
	 * WC Logger object
	 * @var object
	 */
	private static $log;

	/**
	 * Plugin URL
	 * @var type
	 */
	private static $plugin_url;

	/**
	 * Plugin Path
	 * @var string
	 */
	private static $plugin_path;

	/**
	 * Is WC Subscriptions active
	 * @since 2.0
	 * @var bool
	 */
	private static $is_subscriptions_active;

	/**
	 * WC Subscriptions version
	 * @since 2.0
	 * @var bool
	 */
	private static $is_subscriptions_version;

	/**
	 * Is WC Pre-Orders active
	 * @since 2.0
	 * @var bool
	 */
	private static $is_pre_orders_active;

	/**
	 * Is debug mode enabled
	 * @var bool
	 */
	private static $is_debug_enabled;

	public function __construct() {
		$this->load_gateway_files();

		$this->add_autoloader();

		// Add a 'Settings' link to the plugin action links
		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'settings_support_link' ), 10, 4 );

		add_action( 'init', array( $this, 'load_text_domain' ) );

		add_filter( 'woocommerce_payment_gateways', array( $this, 'add_payment_gateway' ) );

		add_action( 'init', array( $this, 'cancel_url_response_check' ) );

		add_action( 'template_redirect', array( $this, 'return_url_response_check' ) );
	}

	/**
	 * Add the plugin autoloader for class files
	 *
	 * @since 2.0
	 */
	public function add_autoloader() {
		require_once( 'includes/class-wc-netbanx-autoloader.php' );
		$loader = new WC_Netbanx_Autoloader( self::plugin_path() );
		spl_autoload_register( array( $loader, 'load_classes' ) );
	}

	/**
	 * Localisation
	 **/
	public function load_text_domain() {
		load_plugin_textdomain( 'wc_netbanx', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}

	/**
	 * Add 'Settings' link to the plugin actions links
	 *
	 * @return array associative array of plugin action links
	 */
	public function settings_support_link( $actions, $plugin_file, $plugin_data, $context ) {

		$gateway = $this->get_gateway_class();

		return array_merge(
			array( 'settings' => '<a href="' . WC_Compat_Netbanx::gateway_settings_page( $gateway ) . '">' . __( 'Settings', 'wc_netbanx' ) . '</a>' ),
			$actions
		);
	}

	/**
	 * Get the correct gateway class name to load
	 *
	 * @return string Class name
	 */
	private function get_gateway_class() {
		if ( self::is_subscriptions_active() || self::is_pre_orders_active() ) {
			// Support for WooCommerce Subscriptions 1.n
			if ( 1 === self::get_subscriptions_version() ) {
				$methods = 'WC_Netbanx_Gateway_Addons_Deprecated';
			} else {
				$methods = 'WC_Netbanx_Gateway_Addons';
			}
		} else {
			$methods = 'WC_Netbanx_Gateway';
		}

		return $methods;
	}

	/**
	 * Load gateway files
	 *
	 * Legacy classes that need to be included manually
	 */
	public function load_gateway_files() {
		include_once( 'includes/class-wc-compat-netbanx.php' );
		include_once( 'includes/class-wc-gateway-netbanx.php' );
	}

	/**
	 * Safely get POST variables
	 *
	 * @since      1.0
	 *
	 * @param string $name POST variable name
	 *
	 * @deprecated 1.2. Use get_field method instead
	 *
	 * @return string The variable value
	 */
	public static function get_post( $name ) {
		if ( isset( $_POST[ $name ] ) ) {
			return $_POST[ $name ];
		}

		return '';
	}

	/**
	 * Safely get GET variables
	 *
	 * @since      1.0
	 *
	 * @param string $name GET variable name
	 *
	 * @deprecated 1.2. Use get_field method instead
	 *
	 * @return string The variable value
	 */
	public static function get_get( $name ) {
		if ( isset( $_GET[ $name ] ) ) {
			return $_GET[ $name ];
		}

		return '';
	}

	/**
	 * Safely get GET variables
	 *
	 * @since 2.0
	 *
	 * @param string $name GET variable name
	 *
	 * @return string The variable value
	 */
	public static function get_field( $name, array $array, $default = '' ) {
		if ( isset( $array[ $name ] ) ) {
			return $array[ $name ];
		}

		return $default;
	}

	/**
	 * Add the gateway to WooCommerce
	 *
	 * @param array $methods
	 *
	 * @return array
	 */
	function add_payment_gateway( $methods ) {
		$methods[] = $this->get_gateway_class();

		return $methods;
	}

	/**
	 * Add debug log message
	 *
	 * @param string $message
	 */
	public static function add_debug_log( $message ) {
		if ( ! is_object( self::$log ) ) {
			self::$log = WC_Compat_Netbanx::get_wc_logger();
		}

		if ( self::is_debug_enabled() ) {
			self::$log->add( 'netbanx', $message );
		}
	}

	/**
	 * Check, if debug logging is enabled
	 *
	 * @since 1.1.2
	 * @return bool
	 */
	public static function is_debug_enabled() {
		if ( self::$is_debug_enabled ) {
			return self::$is_debug_enabled;
		} else {
			$settings = get_option( 'woocommerce_netbanx_settings' );

			self::$is_debug_enabled = ( 'yes' == self::get_field( 'debug', $settings, 'no' ) );

			return self::$is_debug_enabled;
		}
	}

	/**
	 * Get the plugin url
	 *
	 * @return string
	 */
	public static function plugin_url() {
		if ( self::$plugin_url ) {
			return self::$plugin_url;
		}

		return self::$plugin_url = untrailingslashit( plugins_url( '/', __FILE__ ) );
	}

	/**
	 * Get the plugin path
	 *
	 * @return string
	 */
	public static function plugin_path() {
		if ( self::$plugin_path ) {
			return self::$plugin_path;
		}

		return self::$plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
	}

	/**
	 * Return the order number with stripped # or n° ( french translations )
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public static function get_order_number( WC_Order $order ) {
		return str_replace( array( '#', 'n°' ), '', $order->get_order_number() );
	}

	/**
	 * Redirect the Customer to the Cancel order page.
	 * Used for iframe only, to escape from the iframe and return the customer to the main window.
	 *
	 * @since 2.0
	 */
	function cancel_url_response_check() {
		if ( self::get_field( 'cancel_order', $_GET )
			&& self::get_field( 'order', $_GET )
			&& self::get_field( 'order_id', $_GET )
			&& 'true' == self::get_field( 'netbanx-hosted-return-cancel', $_GET )
		) {
			$order = WC_Compat_Netbanx::wc_get_order( (int) self::get_field( 'order_id', $_GET ) );

			$redirect_url = $order->get_cancel_order_url();
			echo "<script>window.parent.location.href='" . $redirect_url . "';</script>";
			exit;
		}
	}

	/**
	 * Redirect the thank you page.
	 * Used for iframe only, to escape from the iframe and return the customer to the main window.
	 *
	 * @since 2.0
	 */
	function return_url_response_check() {
		if ( is_order_received_page() && self::get_field( 'netbanx-hosted-return', $_GET ) ) {
			$init_gateway = new WC_Netbanx_Gateway();
			$order        = WC_Compat_Netbanx::wc_get_order( (int) self::get_field( 'netbanx-hosted-return', $_GET ) );

			$redirect_url = $init_gateway->get_return_url( $order );
			echo "<script>window.parent.location.href='" . $redirect_url . "';</script>";
			exit;
		}
	}

	/**
	 * Detect if WC Subscriptions is active
	 *
	 * @since 2.0
	 * @return bool True if active, False if not
	 */
	public static function is_subscriptions_active() {
		if ( is_bool( self::$is_subscriptions_active ) ) {
			return self::$is_subscriptions_active;
		}

		self::$is_subscriptions_active = false;

		if ( class_exists( 'WC_Subscriptions' ) || function_exists( 'wcs_order_contains_subscription' ) ) {
			self::$is_subscriptions_active = true;
		}

		return self::$is_subscriptions_active;
	}

	/**
	 * Get back the Subsctiptions version.
	 *
	 * @since 2.0
	 * @return bool Main Subscriptions version number (e.i. 1, 2, 3), False, if Subscriptions is not active
	 */
	public static function get_subscriptions_version() {
		if ( null !== self::$is_subscriptions_version ) {
			return self::$is_subscriptions_version;
		}

		self::$is_subscriptions_version = false;

		if ( function_exists( 'wcs_order_contains_subscription' ) ) {
			self::$is_subscriptions_version = 2;
		} elseif ( class_exists( 'WC_Subscriptions' ) ) {
			self::$is_subscriptions_version = 1;
		}

		return self::$is_subscriptions_version;
	}

	/**
	 * Detect if Pre-Orders is active
	 *
	 * @since 2.0
	 * @return bool True if active, False if not
	 */
	public static function is_pre_orders_active() {
		if ( is_bool( self::$is_pre_orders_active ) ) {
			return self::$is_pre_orders_active;
		}

		self::$is_pre_orders_active = false;

		if ( class_exists( 'WC_Pre_Orders' ) ) {
			self::$is_pre_orders_active = true;
		}

		return self::$is_pre_orders_active;
	}
}

add_action( 'plugins_loaded', 'wc_netbanx_gateway_load' );
function wc_netbanx_gateway_load() {
	if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
		return;
	}

	new WC_Netbanx();
}
