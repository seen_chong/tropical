<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Abstract class handling requests to the Netbanx servers.
 *
 *
 * @since  2.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
abstract class WC_Netbanx_Request extends WC_Netbanx_Hosted {

	private $profile_id = '';

	private $profile_token = '';

	/**
	 * Setup the Netbanx payment request
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function build_payment_request( WC_Order $order ) {
		// Debug log
		WC_Netbanx::add_debug_log( 'Generating payment form for order #' . $order->id );

		$amount = $this->format_amount( $order->get_total() );

		$request_params = array(
			'merchantRefNum'            => WC_Netbanx::get_order_number( $order ) . '_' . $this->get_attempts_suffix( $order ),
			'totalAmount'               => $amount,
			'currencyCode'              => WC_Compat_Netbanx::get_order_currency( $order ),
			'customerNotificationEmail' => $order->billing_email,
			'customerIp'                => $this->get_user_ip_addr(),
			'locale'                    => $this->get_locale( $order ),
			'billingDetails'            => $this->get_billing_fields( $order ),
			'callback'                  => $this->get_callback_fields(),
			'addendumData'              => $this->get_addendum_data_fields( $order ),
			'link'                      => $this->get_link_fields( $order ),
		);

		// Add shipping fields
		if ( '' !== $order->shipping_address_1 ) {
			$request_params['shippingDetails'] = $this->get_shipping_fields( $order );
		}

		// Add order details
		if ( $this->maybe_send_order_details() ) {
			// If prices include tax send the order as one item with all products in the name
			if ( 'yes' == $order->prices_include_tax ) {
				$request_params['shoppingCart'] = $this->get_shopping_cart_fields( $order, 'including' );
			} else {
				$request_params['shoppingCart'] = $this->get_shopping_cart_fields( $order, 'excluding' );

				// Add fees only, if needed.
				if ( 0 < $order->get_total_discount()
					|| 0 < $order->get_total_tax()
					|| 0 < WC_Compat_Netbanx::get_total_shipping( $order )
					|| 0 < count( $order->get_items( 'fee' ) )
				) {
					$request_params['ancillaryFees'] = $this->get_ancillary_fees_fields( $order );
				}
			}
		}

		// If we don't have to charge a payment. Total 0
		if ( 0 == $amount ) {
			$request_params['extendedOptions'][] = array(
				'key'   => 'authType',
				'value' => 'auth',
			);
		}

		// Add the customer profile node
		$request_params['profile'] = $this->add_customer_profile_fields( $order );

		return $request_params;
	}

	/**
	 * Process an order payment request.
	 * The request sets up a payment and returns an object with the payment URL link.
	 *
	 * @since 2.0
	 *
	 * @param          $params
	 *
	 * @return OptimalPayments\HostedPayment\Order
	 */
	public function process_order( $params ) {
		// Debug log
		WC_Netbanx::add_debug_log( 'Payment request: ' . print_r( $params, true ) );

		// Send request to setup the payment token
		$response = $this->get_client()->hostedPaymentService()->processOrder(
			new OptimalPayments\HostedPayment\Order( $params )
		);

		// Debug log
		WC_Netbanx::add_debug_log( 'Payment response: ' . print_r( $response, true ) );

		return $response;
	}

	/**
	 * Process an order payment refund.
	 *
	 * @since 2.0
	 *
	 * @param          $params
	 *
	 * @return OptimalPayments\HostedPayment\Refund
	 */
	public function process_refund( $params ) {
		// Debug log
		WC_Netbanx::add_debug_log( 'Refund request: ' . print_r( $params, true ) );

		// Send request to setup the payment token
		$refund = $this->get_client()->hostedPaymentService()->refund(
			new OptimalPayments\HostedPayment\Refund( $params )
		);

		// Debug log
		WC_Netbanx::add_debug_log( 'Refund response: ' . print_r( $refund, true ) );

		return $refund;
	}

	/**
	 * Process order look up.
	 *
	 * @since 2.0
	 *
	 * @param $id
	 *
	 * @return OptimalPayments\HostedPayment\Order
	 */
	public function process_order_lookup( $id ) {
		$params = array(
			'id' => $id,
		);

		// Send request to setup the payment token
		$lookup = $this->get_client()->hostedPaymentService()->getOrder(
			new OptimalPayments\HostedPayment\Order( $params )
		);

		return $lookup;
	}

	/**
	 * Process a rebill payment
	 *
	 * @since 2.0
	 *
	 * @param array $params
	 *
	 * @return OptimalPayments\HostedPayment\Order
	 */
	public function process_rebill( $params ) {

		WC_Netbanx::add_debug_log( 'Rebill request: ' . print_r( $params, true ) );

		// Send request to setup the payment token
		$rebill = $this->get_client()->hostedPaymentService()->rebillOrder(
			new OptimalPayments\HostedPayment\Order( $params )
		);

		WC_Netbanx::add_debug_log( 'Rebill response: ' . print_r( $rebill, true ) );

		return $rebill;
	}

	/**
	 * Attempt to process an order.
	 * It will run two attempts to process an order.
	 * If the first attempt fails because of a already used profile merchantCustomerId,
	 * it will increment the merchantCustomerId suffix and attempt the order again.
	 * This is done because of instances where a first attempt on payment fails,
	 * but even though a profile was successful it was not saved to the customer.
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 * @param array    $params The Request fields
	 *
	 * @return \OptimalPayments\HostedPayment\Order
	 */
	public function attempt_to_process_order( WC_Order $order, $params ) {
		for ( $i = 1; $i <= 2; $i ++ ) {
			try {
				$response = $this->process_order( $params );

				// Save the payment profile after the payment request.
				$this->save_customer_payment_profile( $order, $response );

				break;
			}
			catch ( Exception $e ) {
				WC_Netbanx::add_debug_log( 'Exception is: Code: ' . $e->getCode() );
				WC_Netbanx::add_debug_log( 'Exception is: Message: ' . $e->getMessage() );

				if ( '7505' == $e->getCode() && 1 === $i ) {

					WC_Netbanx::add_debug_log( 'Re-running: ' . $e->getCode() );

					// Increment the user profile suffix and go on to the next attempt
					$this->increment_user_profile_id_suffix( $order->user_id );

					continue;
				} else {
					// Throw any other exceptions the same method they came in.
					$exception = get_class( $e );
					throw new $exception( $e->getMessage(), $e->getCode() );
				}
			}
		}

		return $response;
	}

	/**
	 * Increment the user profile suffix.
	 * Suffix is mostly used when there where problems with creation of user profile.
	 *
	 * @since 2.0
	 *
	 * @param int $user_id
	 *
	 * @return mixed|void
	 */
	public function increment_user_profile_id_suffix( $user_id ) {
		// We only increment registered users.
		// Guests are unique ids, so we can just retry
		if ( 0 === (int) $user_id ) {
			return;
		}

		$current_suffix = $this->get_merchant_profile_id_suffix( $user_id );
		$increment      = absint( $current_suffix ) + 1;

		return $this->update_merchant_profile_id_suffix( $user_id, $increment );
	}

	/**
	 * Does the user have a Netbanx profile on file
	 *
	 * @since 2.0
	 *
	 * @return bool
	 */
	public function user_has_netbanx_profile() {
		if ( '' != $this->get_profile_id() ) {
			return true;
		}

		return false;
	}

	/**
	 * Returns order suffix, to prevent duplicate order reference numbers
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function get_attempts_suffix( WC_Order $order ) {
		// Add a retry count sufix to the orderID.
		$attempts        = get_post_meta( $order->id, '_netbanx_order_payment_attempts', true );
		$attempts_suffix = 0;

		if ( is_numeric( $attempts ) ) {
			$attempts_suffix = $attempts;

			$attempts_suffix ++;
		}

		// Save the incremented attempts
		update_post_meta( $order->id, '_netbanx_order_payment_attempts', $attempts_suffix );

		return $attempts_suffix;
	}

	/**
	 * Returns if the callback should be synchronous or not
	 *
	 * @since 2.0
	 *
	 * @return bool
	 */
	public function is_synchro() {
		return ( 'yes' == $this->get_gateway()->get_option( 'synchro' ) );
	}

	/**
	 * Returns the customer IP address
	 *
	 * @since 2.0
	 *
	 * @return type
	 */
	public function get_user_ip_addr() {
		if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		return $ip;
	}

	/**
	 * Returns the set request locale
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return mixed|void
	 */
	public function get_locale( WC_Order $order ) {
		return apply_filters( 'wc_netbanx_locale', $this->get_gateway()->get_option( 'locale' ), $order );
	}

	/**
	 * Do we send the order details to netbanx
	 *
	 * @since 2.0
	 *
	 * @return bool
	 */
	public function maybe_send_order_details() {
		return ( 'yes' == $this->get_gateway()->get_option( 'send_order_details' ) );
	}

	/**
	 * Sets the Netbanx profile ID to class variable
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 */
	private function set_profile_id( WC_Order $order ) {
		$user_profile_id = $this->get_user_profile_id( $order->user_id );

		// Check the user for profile ID
		if ( '' != $user_profile_id ) {
			$this->profile_id = $user_profile_id;
		} else {
			// Check the order for profile ID
			$order_profile_id = $this->get_order_profile_id( $order->user_id );
			if ( '' != $order_profile_id ) {
				$this->profile_id = $order_profile_id;
			}
		}
	}

	/**
	 * Sets the Netbanx profile token to class variable
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 */
	private function set_profile_token( WC_Order $order ) {
		$user_profile_token = $this->get_user_profile_token( $order->user_id );

		// Check the user for profile Token
		if ( '' != $user_profile_token ) {
			$this->profile_token = $user_profile_token;
		} else {
			// Check the order for profile Token
			$order_profile_token = $this->get_order_profile_token( $order->user_id );
			if ( '' != $order_profile_token ) {
				$this->profile_token = $order_profile_token;
			}
		}
	}

	/**
	 * Returns Netbanx profile ID
	 *
	 * @since 2.0
	 *
	 * @return null
	 */
	public function get_profile_id() {
		return $this->profile_id;
	}

	/**
	 * Returns Netbanx profile token
	 *
	 * @since 2.0
	 *
	 * @return null
	 */
	public function get_profile_token() {
		return $this->profile_token;
	}

	/**
	 * Add a customer profile to the payment request
	 *
	 * Profile should be two types:
	 *
	 * I - Saved to the user itself
	 *        1. We can use it to process future payments for that user
	 * II - Saved to the order itself
	 *        1. In cases where the user is a guest, we will save the profile and token to the order
	 *        2. We can process that profile and move it from order to order or Subs to Subs
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return array Profile element fields
	 */
	public function add_customer_profile_fields( WC_Order $order ) {
		$this->set_profile_id( $order );
		$this->set_profile_token( $order );

		WC_Netbanx::add_debug_log( 'Profile ID after initial request setup: ' . $this->get_profile_id() );
		WC_Netbanx::add_debug_log( 'Profile Token after initial request setup: ' . $this->get_profile_token() );

		// We can find a profile ID to use for this order
		if ( $this->user_has_netbanx_profile() ) {
			$profile['id'] = $this->get_profile_id();

			if ( '' != $this->get_profile_token() ) {
				$profile['paymentToken'] = $this->get_profile_token();
			} else {
				$profile['firstName'] = $order->billing_first_name;
				$profile['lastName']  = $order->billing_last_name;
			}
		} else {
			// No Profile ID, so lets create one for the user/guest
			$id = $this->get_merchant_customer_id( $order->user_id );

			$profile = array(
				'merchantCustomerId' => $id,
				'firstName'          => $order->billing_first_name,
				'lastName'           => $order->billing_last_name,
			);
		}

		return $profile;
	}

	/**
	 * Returns the payment request billing fields
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return array
	 */
	public function get_billing_fields( WC_Order $order ) {
		$billing = array(
			'city'    => $order->billing_city,
			'country' => $order->billing_country,
			'street'  => $order->billing_address_1,
			'zip'     => $order->billing_postcode,
			'state'   => '' == $order->billing_state ? $order->billing_city : $order->billing_state,
			'phone'   => $order->billing_phone,
		);

		if ( '' != $order->billing_address_2 ) {
			$billing['street2'] = $order->billing_address_2;
		}

		return $billing;
	}

	/**
	 * Returns the payment request callback fields
	 *
	 * @since 2.0
	 *
	 * @return array
	 */
	public function get_callback_fields() {
		return array(
			array(
				'format'      => 'get',
				'rel'         => 'on_success',
				'retries'     => 3,
				'synchronous' => $this->is_synchro(),
				'uri'         => WC_Compat_Netbanx::api_request_url( get_class( $this->get_gateway() ) ),
				'returnKeys'  => array(
					'id',
				),
			),
			array(
				'format'      => 'get',
				'rel'         => 'on_decline',
				'retries'     => 3,
				'synchronous' => $this->is_synchro(),
				'uri'         => WC_Compat_Netbanx::api_request_url( get_class( $this->get_gateway() ) ),
				'returnKeys'  => array(
					'id',
				),
			),
		);
	}

	/**
	 * Returns the payment request addendum data fields
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return array
	 */
	public function get_addendum_data_fields( WC_Order $order ) {
		return array(
			array(
				'key'   => 'order_id',
				'value' => $order->id
			)
		);
	}

	/**
	 * Returns the payment request link fields.
	 * Link fields are used for cancel button url and location to return the customer to.
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_link_fields( WC_Order $order ) {
		$return = $this->get_gateway()->get_return_url( $order );
		$cancel = $order->get_cancel_order_url();

		if ( $this->get_gateway()->is_using_iframe() ) {
			$return = add_query_arg( 'netbanx-hosted-return', $order->id, $return );
			$cancel = add_query_arg( 'netbanx-hosted-return-cancel', $order->id, $cancel );
		}

		return array(
			array(
				'rel' => 'return_url',
				'uri' => $return
			),
			array(
				'rel' => 'cancel_url',
				'uri' => $cancel
			)
		);
	}

	/**
	 * Returns the payment request shipping fields
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return array
	 */
	public function get_shipping_fields( WC_Order $order ) {
		$shipping = array(
			'recipientName' => $order->shipping_first_name . ' ' . $order->shipping_last_name,
			'city'          => $order->shipping_city,
			'country'       => $order->shipping_country,
			'street'        => $order->shipping_address_1,
			'zip'           => $order->shipping_postcode,
			'state'         => '' == $order->shipping_state ? $order->shipping_city : $order->shipping_state,
		);

		if ( '' != $order->shipping_address_2 ) {
			$shipping['street2'] = $order->shipping_address_2;
		}

		return $shipping;
	}

	/**
	 * Returns the payment request shopping cart details fields.
	 * All fields, total amounts + ancillary fees, should match the total amount of payment sent.
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 * @param string   $price_tax_calculation
	 *
	 * @return mixed
	 */
	public function get_shopping_cart_fields( WC_Order $order, $price_tax_calculation = 'including' ) {
		$shopping_cart = array();
		if ( 'including' == $price_tax_calculation ) {
			$desc = '';
			if ( count( $order->get_items() ) ) {
				foreach ( $order->get_items() as $item ) {
					if ( $item['qty'] ) {
						$item_name = $item['name'];
						$item_meta = WC_Compat_Netbanx::get_order_item_meta( $item );

						if ( $meta = $item_meta->display( true, true, '_', ' | ' ) ) {
							$item_name .= ' (' . $meta . ')';
						}

						$desc .= $item['qty'] . ' x ' . $item_name . ', ';
					}
				}
				// Add the description
				$desc = substr( $desc, 0, - 2 );
				$desc = $this->format_string( $desc, 50 );
			}

			$shopping_cart[] = array(
				'amount'      => $this->format_amount( $order->get_total() ),
				'description' => $desc,
				'quantity'    => 1,
			);
		} else {
			// Cart Contents
			if ( 0 < count( $order->get_items() ) ) {
				foreach ( $order->get_items() as $item ) {
					if ( $item['qty'] ) {
						$product   = $order->get_product_from_item( $item );
						$item_name = $item['name'];
						$item_meta = WC_Compat_Netbanx::get_order_item_meta( $item );

						if ( $meta = $item_meta->display( true, true, '_', ' | ' ) ) {
							$item_name .= ' (' . $meta . ')';
						}

						// Cart items
						$shopping_cart[] = array(
							'amount'      => $this->format_amount( $product->get_price() ),
							'description' => $this->format_string( $item_name, 50 ),
							'quantity'    => $item['qty'],
							'sku'         => ( $product->get_sku() ) ? $product->get_sku() : $product->id,
						);
					}
				}
			}
		}

		return $shopping_cart;
	}

	/**
	 * Returns the payment request ancillary fees fields.
	 * Fees are Tax, Discount, Shipping amount.
	 * All fields, ancillary fees + total amount of shopping cart, should match the total amount of payment sent.
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return array
	 */
	public function get_ancillary_fees_fields( WC_Order $order ) {
		$fees = array();

		// Add the order tax
		if ( 0 < $order->get_total_tax() ) {
			$fees[] = array(
				'amount'      => $this->format_amount( $order->get_total_tax() ),
				'description' => $this->format_string( __( 'Tax', 'wc_netbanx' ), 50 ),
			);
		}

		// Add the order discount
		if ( 0 < $order->get_total_discount() ) {
			$fees[] = array(
				'amount'      => - $this->format_amount( $order->get_total_discount() ),
				'description' => $this->format_string( __( 'Discount', 'wc_netbanx' ), 50 ),
			);
		}

		// Add the shipping parameter
		if ( 0 < WC_Compat_Netbanx::get_total_shipping( $order ) ) {
			$fees[] = array(
				'amount'      => $this->format_amount( WC_Compat_Netbanx::get_total_shipping( $order ) ),
				'description' => $this->format_string( __( 'Shipping', 'wc_netbanx' ), 50 ),
			);
		}

		$item_fees = $order->get_items( 'fee' );
		if ( 0 < count( $item_fees ) ) {
			foreach ( $item_fees as $fee ) {
				$fees[] = array(
					'amount'      => $this->format_amount( $fee['line_total'] ),
					'description' => $this->format_string( $fee['name'], 50 ),
				);
			}
		}

		return $fees;
	}

	/**
	 * Returns the payment request profile -> merchantCustomerId.
	 *
	 * @since 2.0
	 *
	 * @param int $user_id
	 *
	 * @return string
	 * @throws Exception
	 */
	public function get_merchant_customer_id( $user_id ) {
		if ( ! empty( $user_id ) ) {
			$suffix_value = $this->get_merchant_profile_id_suffix( $user_id );
			$suffix       = '' == $suffix_value ? '' : '-' . $suffix_value;
			$id           = $this->get_gateway()->get_option( 'user_prefix' ) . $user_id . $suffix;
		} else {
			$id = uniqid( $this->get_gateway()->get_option( 'user_prefix' ), true );
		}

		return $id;
	}

	/**
	 * Returns the merchant customer id suffix
	 *
	 * @since 2.0
	 *
	 * @param int $user_id
	 *
	 * @return mixed
	 */
	public function get_merchant_profile_id_suffix( $user_id ) {
		if ( 0 === (int) $user_id ) {
			return;
		}

		return get_user_meta( $user_id, '_netbanx_hosted_merchant_customer_id_suffix', true );
	}

	/**
	 * Updates the merchant customer id suffix
	 *
	 * @since 2.0
	 *
	 * @param int   $user_id
	 * @param mixed $value
	 *
	 * @return mixed
	 */
	public function update_merchant_profile_id_suffix( $user_id, $value ) {
		if ( 0 === (int) $user_id ) {
			return;
		}

		return update_user_meta( $user_id, '_netbanx_hosted_merchant_customer_id_suffix', $value );
	}
}