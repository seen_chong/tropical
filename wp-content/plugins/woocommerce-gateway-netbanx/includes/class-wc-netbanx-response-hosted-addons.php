<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Description
 *
 * @since  2.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
class WC_Netbanx_Response_Hosted_Addons extends WC_Netbanx_Response {
	/**
	 * Process Hosted API payment response
	 *
	 * @since 2.0
	 * @throws InvalidArgumentException
	 **/
	function process_response() {
		$response = $this->lookup_the_response_and_return_the_result();
	}

	/**
	 * Follows the main procedure of validating and processing a payment response.
	 *
	 * 1. Look up the order in Netbanx system.
	 * 2. Validate the Look up response against the WC order.
	 * 3. Process the WC order based on the Look up response.
	 *
	 * @since 2.0
	 *
	 * @return \OptimalPayments\HostedPayment\Order The Netbanx Look up response object.
	 */
	public function lookup_the_response_and_return_the_result() {
		// Debug log
		WC_Netbanx::add_debug_log( 'Payment response received. Response POST is: ' . print_r( $_POST, true ) );

		// We got a response for a payment, query back the payment
		// to get all information and ensure everything is correct.
		$response = $this->response_order_lookup( WC_Netbanx::get_field( 'id', $_POST, '' ) );

		// Get the order ID from the lookup(requery) object
		$order = $this->get_wc_order_from_response_object( $response );

		if ( $this->get_gateway()->order_contains_subscription( $order ) ) {
			// Validate the response
			$this->validate_payment_response( $order, $response );

			// Process the order based on the response received from Netbanx.
			$this->process_order_based_on_response( $response, $order );

			if ( 'success' == $response->transaction->status ) {
				$this->save_meta_data_to_subscription( $order, $response );
			}
		} elseif ( $this->get_gateway()->order_contains_pre_order( $order ) ) {
			if ( null !== WC_Netbanx::get_field( 'order_id', $_POST, null ) ) {
				$this->validate_order_id( WC_Netbanx::get_field( 'order_id', $_POST, '0' ), $response->id );
			}

			$this->validate_currency( $order->get_order_currency(), $response->currencyCode );
			$this->validate_order_status( $order );

			if ( 'success' == $response->transaction->status ) {
				// Add transaction details to order
				$this->save_transaction_details_to_order( $order, $response );

				// Now that we have the info need for future payment, mark the order pre-ordered
				WC_Pre_Orders_Order::mark_order_as_pre_ordered( $order );
			}
		}

		return $response;
	}

	/**
	 * Save the transaction details to the Subscription
	 *
	 * @since 2.0
	 *
	 * @param $order
	 * @param $response
	 */
	public function save_meta_data_to_subscription( $order, $response ) {
		// Also store it on the subscriptions being purchased or paid for in the order
		if ( wcs_order_contains_subscription( $order ) ) {
			$subscriptions = wcs_get_subscriptions_for_order( $order );
		} elseif ( wcs_order_contains_renewal( $order ) ) {
			$subscriptions = wcs_get_subscriptions_for_renewal_order( $order );
		} else {
			$subscriptions = array();
		}

		foreach ( $subscriptions as $subscription ) {

			// Debug log
			WC_Netbanx::add_debug_log( 'Saving details to subscription: ' . print_r( $subscription->id, true ) );

			$this->save_transaction_details_to_order( $subscription, $response );
		}
	}
}