<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Hosted API payment requests.
 *
 * Will process normal requests for payment of orders containing only normal products.
 *
 * @since  2.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
class WC_Netbanx_Request_Hosted extends WC_Netbanx_Request {

	/**
	 * Returns a payment request URL link.
	 *
	 * It initialize the process of payment request.
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function get_payment_url( WC_Order $order ) {
		// Build
		$params = $this->build_payment_request( $order );

		// Allow for parameters modification
		$params = apply_filters( 'wc_netbanx_request_params', $params, $order );

		$response = $this->attempt_to_process_order( $order, $params );

		return $response->getLink( 'hosted_payment' )->uri;
	}

	/**
	 * Builds the parameters and runs the process refund method.
	 *
	 * @since 2.0
	 *
	 * @param $transaction_id
	 * @param $amount
	 *
	 * @return \OptimalPayments\HostedPayment\Refund
	 */
	public function process_order_refund( $transaction_id, $amount ) {
		$params = array(
			'orderID' => $transaction_id,
			'amount' => $this->format_amount($amount),
		);

		return $this->process_refund( $params );
	}
}