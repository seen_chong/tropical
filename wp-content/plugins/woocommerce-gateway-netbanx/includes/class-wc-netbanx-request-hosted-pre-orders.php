<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Description
 *
 * @since  1.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
class WC_Netbanx_Request_Hosted_Pre_Orders extends WC_Netbanx_Request {
	/**
	 * Returns a payment request URL link.
	 *
	 * It initialize the process of payment request.
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function get_payment_url( WC_Order $order ) {
		// Build
		$params = $this->build_payment_request( $order );

		// Allow for parameters modification
		$params = apply_filters( 'wc_netbanx_addons_request_params', $params, $order );

		$response = $this->attempt_to_process_order( $order, $params );

		return $response->getLink( 'hosted_payment' )->uri;
	}

	public function build_payment_request( $order ) {
		if ( WC_Pre_Orders_Order::order_requires_payment_tokenization( $order ) ) {
			$params = $this->build_authorization_only_request( $order );
		} else {
			$params = parent::build_payment_request( $order );
		}

		return $params;
	}

	/**
	 * Setup the Netbanx payment request
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function build_authorization_only_request( WC_Order $order ) {
		// Debug log
		WC_Netbanx::add_debug_log( 'Generating authorization request for Pre-Order #' . $order->id );

		$amount = 0;

		$request_params = array(
			'merchantRefNum'            => WC_Netbanx::get_order_number( $order ) . '_' . $this->get_attempts_suffix( $order ),
			'totalAmount'               => $amount,
			'currencyCode'              => WC_Compat_Netbanx::get_order_currency( $order ),
			'customerNotificationEmail' => $order->billing_email,
			'customerIp'                => $this->get_user_ip_addr(),
			'locale'                    => $this->get_locale( $order ),
			'billingDetails'            => $this->get_billing_fields( $order ),
			'callback'                  => $this->get_callback_fields(),
			'addendumData'              => $this->get_addendum_data_fields( $order ),
			'link'                      => $this->get_link_fields( $order ),
			'extendedOptions'           => array(
				array(
					'key'   => 'authType',
					'value' => 'auth',
				)
			)
		);

		// Add shipping fields
		if ( '' !== $order->shipping_address_1 ) {
			$request_params['shippingDetails'] = $this->get_shipping_fields( $order );
		}

		// Add order details
		if ( $this->maybe_send_order_details() ) {
			$request_params['shoppingCart'] = $this->get_pre_orders_cart_fields( $order );
		}

		// Add the customer profile node
		$request_params['profile'] = $this->add_customer_profile_fields( $order );

		return $request_params;
	}

	/**
	 * Add the Pre-Orders cart details, so user knows what they are authorizing for.
	 *
	 * @param WC_Order $order
	 *
	 * @return array
	 */
	public function get_pre_orders_cart_fields( $order ) {
		// Cart items
		$shopping_cart[] = array(
			'amount'      => '0',
			'description' => $this->format_string( sprintf( __( 'Pre-Order #%s' ), WC_Netbanx::get_order_number( $order ) ), 50 ),
			'quantity'    => 1,
		);

		return $shopping_cart;
	}

	/**
	 * Process a rebill for a Pre-Order
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return OptimalPayments\HostedPayment\Order
	 */
	public function process_pre_orders_rebill( WC_Order $order ) {
		$transaction_id = $this->get_netbanx_payment_order_id( $order );

		$amount = $order->get_total();

		$params = array(
			'id'             => $transaction_id,
			'totalAmount'    => $this->format_amount( $amount ),
			'currencyCode'   => $order->get_order_currency(),
			'merchantRefNum' => WC_Netbanx::get_order_number( $order ) . '_' . $this->get_attempts_suffix( $order ),
		);

		$params['shoppingCart'] = $this->get_shopping_cart_fields( $order, 'including' );

		return $this->process_rebill( $params );
	}
}