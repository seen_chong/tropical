<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Description
 *
 * @since  2.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
class WC_Netbanx_Gateway extends WC_Payment_Gateway {

	public static $api;

	public function __construct() {

		$this->id           = 'netbanx';
		$this->icon         = apply_filters( 'woocommerce_netbanx_icon', WC_Netbanx::plugin_url() . '/images/netbanx_logo.png' );
		$this->has_fields   = false;
		$this->method_title = __( 'Netbanx', 'wc_netbanx' );

		// Load the form fields.
		$this->init_form_fields();

		// Load the settings.
		$this->init_settings();

		// Define user set variables
		$this->title              = $this->get_option( 'title' );
		$this->description        = $this->get_option( 'description' );
		$this->enabled            = $this->get_option( 'enabled', 'no' );
		$this->testmode           = $this->get_option( 'testmode', 'yes' );
		$this->account_num        = $this->get_option( 'account_num' );
		$this->api_key            = $this->get_option( 'api_key' );
		$this->locale             = $this->get_option( 'locale', 'en_GB' );
		$this->user_prefix        = $this->get_option( 'user_prefix', 'WC-' );
		$this->debug              = $this->get_option( 'debug', 'no' );
		$this->synchro            = $this->get_option( 'synchro', 'no' );
		$this->used_api           = $this->get_option( 'used_api', 'hosted' );
		$this->send_order_details = $this->get_option( 'send_order_details', 'yes' );
		$this->use_iframe         = $this->get_option( 'use_iframe', 'no' );
		$this->iframe_width       = $this->get_option( 'iframe_width', '700' );
		$this->iframe_height      = $this->get_option( 'iframe_height', '850' );
		$this->iframe_scroll      = $this->get_option( 'iframe_scroll', 'yes' );

		$this->supports = array(
			'subscriptions',
			'products',
			'subscription_cancellation',
			'subscription_reactivation',
			'subscription_suspension',
			'subscription_amount_changes',
			'subscription_payment_method_change', // Subs 1.n compatibility
			'subscription_payment_method_change_customer',
			'subscription_payment_method_change_admin',
			'subscription_date_changes',
			'multiple_subscriptions',
			'refunds',
			'pre-orders',
		);

		// Actions
		add_action( 'woocommerce_api_' . strtolower( get_class( $this ) ), array( $this, 'process_response' ) );

		add_action( 'woocommerce_receipt_netbanx', array( $this, 'receipt_page' ) );

		// Save options
		add_action( 'woocommerce_update_options_payment_gateways', array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

		add_action( 'admin_notices', array( $this, 'add_admin_notices' ) );
	}

	/**
	 * Admin Panel Options
	 */
	public function admin_options() {
		?>
		<h3><?php _e( 'Netbanx', 'wc_netbanx' ); ?></h3>
		<p><?php _e( 'Nebtanx gateway uses the Netbanx Hosted API to take payments from your customers. You can use an iframe or simply redirect your customers to display the payment form. The plugin also supports Refunds, WooCommerce Subscriptions and WooCommerce Pre-Orders.', 'wc_netbanx' ); ?></p>

		<table class="form-table">
			<?php
			// Generate the HTML For the settings form.
			$this->generate_settings_html();
			?>
		</table><!--/.form-table-->
		<?php
		$javascript = "
			$('#woocommerce_netbanx_use_iframe').change(
				function() {
					var width = $(this).closest('tr').next();
					var height = width.next();
					var scroll = height.next();

					if ( $(this).is(':checked') ) {
						width.show();
						height.show();
						scroll.show();
					} else {
						width.hide();
						height.hide();
						scroll.hide();
					}
				}
			).change();
		";
		WC_Compat_Netbanx::wc_include_js( $javascript );
	} // End admin_options()

	/**
	 * Initialise Gateway Settings Form Fields
	 **/
	public function init_form_fields() {

		$this->form_fields = array(
			'enabled'            => array(
				'title'   => __( 'Enable/Disable', 'wc_netbanx' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable Netbanx', 'wc_netbanx' ),
				'default' => 'no'
			),
			'testmode'           => array(
				'title'   => __( 'Sandbox', 'wc_netbanx' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable Sandbox', 'wc_netbanx' ),
				'default' => 'yes'
			),
			'title'              => array(
				'title'       => __( 'Method Title', 'wc_netbanx' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'wc_netbanx' ),
				'default'     => __( 'Netbanx', 'wc_netbanx' )
			),
			'description'        => array(
				'title'       => __( 'Description', 'wc_netbanx' ),
				'type'        => 'textarea',
				'description' => __( 'This controls the description which the user sees during checkout.', 'wc_netbanx' ),
				'default'     => __( "Pay using your credit/debit card.", 'wc_netbanx' )
			),
			'api_key'            => array(
				'title'       => __( 'API Key', 'wc_netbanx' ),
				'type'        => 'password',
				'description' => __( "Enter the API Key given to you by Netbanx for API connection. The API Key field will always appear empty. Just enter the API Key, when you want to set it. Leave empty otherwise.", 'wc_netbanx' ),
				'placeholder' => 'Enter API Key',
				'default'     => ''
			),
			'locale'             => array(
				'title'       => __( 'Payment Pages Language', 'wc_netbanx' ),
				'type'        => 'select',
				'description' => __( 'Choose the language you want your Netbanx payment pages to be in.', 'wc_netbanx' ),
				'default'     => 'en_GB',
				'options'     => array( 'en_GB' => __( 'British English', 'wc_netbanx' ), 'en_US' => __( 'US English', 'wc_netbanx' ), 'fr_FR' => __( 'French', 'wc_netbanx' ), 'fr_CA' => __( 'French Canadian', 'wc_netbanx' ) ),
			),
			'send_order_details' => array(
				'title'       => __( 'Send Order Details', 'wc_netbanx' ),
				'type'        => 'checkbox',
				'label'       => __( 'Send Detailed Order to Netbanx', 'wc_netbanx' ),
				'description' => __( '<strong>Enable</strong>, if you want to send a breakdown of all order details (items, taxes, shipping costs) to Netbanx.<br/><strong>Disable</strong>, if you want to just send the Order Total for the customer to pay.', 'wc_netbanx' ),
				'default'     => 'yes'
			),
			'synchro'            => array(
				'title'       => __( 'Synchronous responses', 'wc_netbanx' ),
				'type'        => 'checkbox',
				'label'       => __( 'Use Synchronous payment responses', 'wc_netbanx' ),
				'description' => __( '<strong>Enable</strong>, payment response will be made synchronously, in-line with the customer returning to your store.<br/><strong>Disable</strong>, payment responses will be made asynchronously, will be delayed a bit, allowing the callback system can detect problems with the merchant system and retry any failed attempts until a successful response is received.<br/><strong>Recommended from NETBANX</strong>: Disable.', 'wc_netbanx' ),
				'default'     => 'no'
			),
			'user_prefix'        => array(
				'title'       => __( 'Customer ID Prefix', 'wc_netbanx' ),
				'type'        => 'text',
				'description' => __( 'Enter here a prefix for the customer ID send to Netbanx. Because the customer ID is required to be unique, we will enter a prefix for it, to make sure it will be. (Example: WC-)', 'wc_netbanx' ),
				'default'     => 'WC-'
			),
			'debug'              => array(
				'title'       => __( 'Debug Log', 'wc_netbanx' ),
				'type'        => 'checkbox',
				'label'       => __( 'Recommended: Test Mode Only', 'wc_netbanx' ),
				'default'     => 'no',
				'description' => sprintf(
					__(
						'Debug log will provide you with most of the data and
				 	events generated by the payment process. Logged inside %s'
					),
					'<code>' . WC_Compat_Netbanx::wc_get_debug_file_path( 'netbanx' ) . '</code>'
				),
			),
			'iframe_section'     => array(
				'title'       => __( 'Iframe Settings', 'wc_netbanx' ),
				'type'        => 'title',
				'description' => __( 'In this section you will set the Iframe control settings.', 'wc_netbanx' ),
			),
			'use_iframe'         => array(
				'title'       => __( 'Enable Iframe', 'wc_netbanx' ),
				'type'        => 'checkbox',
				'description' => __( 'Enable iframe to have the customer payment form display in an Iframe on your site. If disabled, your customers will be redirected to Netbanx payment page.', 'wc_netbanx' ),

				'default'     => 'no'
			),
			'iframe_width'       => array(
				'title'       => __( 'Iframe Width', 'wc_netbanx' ),
				'type'        => 'text',
				'description' => __( 'Width of the iframe window. Enter only numbers in pixels (i.e. 700) or you can enter number in percentage but you need to suffix it with "%" (i.e. 55%).', 'wc_netbanx' ),
				'default'     => '700'
			),
			'iframe_height'      => array(
				'title'       => __( 'Iframe Height', 'wc_netbanx' ),
				'type'        => 'text',
				'description' => __( 'Height of the iframe window. Entered can be a number in pixels (i.e. 850) or you can enter number in percentage but you need to suffix it with "%" (i.e. 55%).', 'wc_netbanx' ),
				'default'     => '850'
			),
			'iframe_scroll'      => array(
				'title'       => __( 'Iframe Scroll', 'wc_netbanx' ),
				'type'        => 'checkbox',
				'description' => __( 'Should the iframe be scrollable or not. If scrollable, the customer will be able to scroll to the iframe to reach its borders.', 'wc_netbanx' ),

				'default'     => 'yes'
			),
		);

	} // End init_form_fields()


	/**
	 * Process the payment and return the result
	 *
	 * @since 1.1
	 *
	 * @param int $order_id
	 *
	 * @return array
	 */
	public function process_payment( $order_id ) {
		try {
			$order = WC_Compat_Netbanx::wc_get_order( $order_id );

			if ( $this->is_using_iframe() ) {
				$url = $order->get_checkout_payment_url( true );
			} else {
				$request = new WC_Netbanx_Request_Hosted( $this );
				$url     = $request->get_payment_url( $order );
			}

			ob_clean();

			return array(
				'result'   => 'success',
				'redirect' => $url
			);
		}
		catch ( Exception $e ) {
			// Any exception is logged and flags a notice
			WC_Netbanx::add_debug_log( 'Payment response error: ' . $e->getMessage() );

			WC_Compat_Netbanx::wc_add_notice( $e->getMessage(), 'error' );
		}
	}

	/**
	 * Call the iframe generation method
	 *
	 * @param int $order_id
	 */
	public function receipt_page( $order_id ) {

		echo '<p>' . $this->string_pay_with_form_below() . '</p>';

		echo $this->generate_netbanx_iframe( $order_id );

	}

	/**
	 * Display the Netbanx payment iframe
	 *
	 * @param int $order_id
	 *
	 * @return string
	 */
	public function generate_netbanx_iframe( $order_id ) {
		try {
			$order = WC_Compat_Netbanx::wc_get_order( $order_id );

			$request  = new WC_Netbanx_Request_Hosted( $this );
			$location = $request->get_payment_url( $order );

			return $this->load_iframe_template( $location );
		}
		catch ( Exception $e ) {
			$this->iframe_error_notification_message( $e->getMessage() );

		}
	}

	/**
	 * Check the payment response and process the order
	 */
	public function process_response() {
		try {
			$response = new WC_Netbanx_Response_Hosted( $this );
			$response->process_response();

			WC_Compat_Netbanx::empty_cart();
		}
		catch ( Exception $ex ) {
			// Debug log
			WC_Netbanx::add_debug_log( $ex->getMessage() );

			$this->end_by_failed_validation();
		}

		// Return 204 status
		header( "HTTP/1.0 204 No Content" );
		exit;
	}

	/**
	 * Process automatic refunds
	 *
	 * @since 2.0
	 *
	 * @param int    $order_id
	 * @param null   $amount
	 * @param string $reason
	 *
	 * @return bool|WP_Error
	 */
	public function process_refund( $order_id, $amount = null, $reason = '' ) {
		try {
			$order = WC_Compat_Netbanx::wc_get_order( $order_id );

			$request        = new WC_Netbanx_Request_Hosted( $this );
			$transaction_id = $request->get_netbanx_payment_order_id( $order );
			if ( '' == $transaction_id ) {
				throw new Exception( __( 'The order does not have a valid Netbanx transaction ID. Refund is not possible.', 'wc_netbanx' ) );
			}

			$refund   = $request->process_order_refund( $transaction_id, $amount );
			$response = new WC_Netbanx_Response_Hosted( $this );
			$response->process_refund_response( $refund, $order, $amount, $transaction_id );

			// Debug log
			WC_Netbanx::add_debug_log( 'Refund completed.' );

			// Add order note
			$order->add_order_note(
				sprintf(
					__(
						'Refunded %s. Refund ID: %s. %s',
						'wc_netbanx'
					),
					$amount,
					$refund->confirmationNumber,
					( '' != $reason ) ? sprintf(
						__(
							'Credit Note: %s.', 'wc_netbanx'
						), $reason
					) : ''
				)
			);

			return true;
		}
		catch ( Exception $ex ) {
			return new WP_Error( 'netbanx-error', $ex->getMessage() );
		}

		return false;
	}

	/**
	 * Is Available for the plugin.
	 *
	 * @return boolean
	 */
	public function is_available() {
		$is_available = parent::is_available();

		if ( empty( $this->api_key ) ) {
			$is_available = false;
		}

		return $is_available;
	}

	/**
	 * Check if SSL is forced on the checkout page.
	 */
	public function add_admin_notices() {
		if ( 'no' == $this->enabled ) {
			return;
		}

		if ( empty( $this->api_key ) ) {
			?>
			<div class="error">
				<p>
					<?php echo sprintf(
						__(
							'Your Netbanx API credentials are not entered.
					 	Please visit the %sNetbanx settings page%s to enter your credentials.
					  	Your gateway will not be shown on the checkout page.', 'wc_netbanx'
						),
						'<a href="' . WC_Compat_Netbanx::gateway_settings_page( get_class( $this ) ) . '">',
						'</a>'
					); ?>
				</p>
			</div>
			<?php
			return;
		}

		if ( 'no' == get_option( 'woocommerce_force_ssl_checkout' ) && ! class_exists( 'WordPressHTTPS' ) && 'no' == $this->testmode ) {
			$message = __(
				'Netbanx is enabled for live payments, but your checkout page is not secured with SSL.
				Your checkout may not be secure.', 'wc_netbanx'
			);

			$message_cont = sprintf(
				__(
					'Please enable the %s"Force secure checkout"%s option or ensure you server has a valid SSL certificate.', 'wc_netbanx'
				), '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout' ) . '">', '</a>'
			);

			echo sprintf( '<div class="error"><p>%s<br/>%s</p></div>', $message, $message_cont );
		}

	}

	/**
	 * Validate Password Field.
	 *
	 * Make sure the data is escaped correctly, etc.
	 * We are not showing the password value to the front end,
	 * so we will overwrite the password validation, so we can update the password only when it is not empty.
	 * If left empty the password will be saved with the old value.
	 *
	 * @access public
	 *
	 * @param mixed $key
	 *
	 * @since  1.1
	 * @return string
	 */
	public function validate_password_field( $key ) {
		$text = $this->get_option( $key );

		if ( isset( $_POST[ $this->plugin_id . $this->id . '_' . $key ] ) && '' != $_POST[ $this->plugin_id . $this->id . '_' . $key ] ) {
			$text = WC_Compat_Netbanx::wc_clean( stripslashes( $_POST[ $this->plugin_id . $this->id . '_' . $key ] ) );
		}

		return $text;
	}

	/**
	 * Generate Password Input HTML.
	 * Overwrite here so it is accessible for WC 2.0
	 *
	 * @access public
	 *
	 * @param mixed $key
	 * @param mixed $data
	 *
	 * @since  1.1
	 * @return string
	 */
	public function generate_password_html( $key, $data ) {
		$data['type'] = 'password';

		return $this->generate_text_html( $key, $data );
	}

	/**
	 * Generate Text Input HTML.
	 * Modify the text html to remove the password value from the front end.
	 *
	 * @access public
	 *
	 * @param mixed $key
	 * @param mixed $data
	 *
	 * @since  1.1
	 * @return string
	 */
	public function generate_text_html( $key, $data ) {
		$field    = $this->plugin_id . $this->id . '_' . $key;
		$defaults = array(
			'title'             => '',
			'disabled'          => false,
			'class'             => '',
			'css'               => '',
			'placeholder'       => '',
			'type'              => 'text',
			'desc_tip'          => false,
			'description'       => '',
			'custom_attributes' => array()
		);

		$data = wp_parse_args( $data, $defaults );

		If ( 'password' == $data['type'] ) {
			$value = '';
		} else {
			$value = esc_attr( $this->get_option( $key ) );
		}

		ob_start();
		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<label for="<?php echo esc_attr( $field ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
				<?php echo $this->get_tooltip_html( $data ); ?>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text">
						<span><?php echo wp_kses_post( $data['title'] ); ?></span></legend>
					<input class="input-text regular-input <?php echo esc_attr( $data['class'] ); ?>" type="<?php echo esc_attr( $data['type'] ); ?>" name="<?php echo esc_attr( $field ); ?>" id="<?php echo esc_attr( $field ); ?>" style="<?php echo esc_attr( $data['css'] ); ?>" value="<?php echo $value; ?>" placeholder="<?php echo esc_attr( $data['placeholder'] ); ?>" <?php disabled( $data['disabled'], true ); ?> <?php echo $this->get_custom_attribute_html( $data ); ?> />
					<?php echo $this->get_description_html( $data ); ?>
				</fieldset>
			</td>
		</tr>
		<?php
		return ob_get_clean();
	}

	/**
	 * Get HTML for tooltips
	 * Overwrite here so it is accessible for WC 2.0
	 *
	 * @since 1.1
	 *
	 * @param  array $data
	 *
	 * @return string
	 */
	public function get_tooltip_html( $data ) {
		if ( $data['desc_tip'] === true ) {
			$tip = $data['description'];
		} elseif ( ! empty( $data['desc_tip'] ) ) {
			$tip = $data['desc_tip'];
		} else {
			$tip = '';
		}

		return $tip ? '<img class="help_tip" data-tip="' . esc_attr( $tip ) . '" src="' . WC_Compat_Netbanx::get_wc_global()->plugin_url() . '/assets/images/help.png" height="16" width="16" />' : '';
	}

	/**
	 * Get HTML for descriptions
	 * Overwrite here so it is accessible for WC 2.0
	 *
	 * @since 1.1
	 *
	 * @param  array $data
	 *
	 * @return string
	 */
	public function get_description_html( $data ) {
		if ( $data['desc_tip'] === true ) {
			$description = '';
		} elseif ( ! empty( $data['desc_tip'] ) ) {
			$description = $data['description'];
		} elseif ( ! empty( $data['description'] ) ) {
			$description = $data['description'];
		} else {
			$description = '';
		}

		return $description ? '<p class="description">' . wp_kses_post( $description ) . '</p>' . "\n" : '';
	}

	/**
	 * Get custom attributes
	 * Overwrite here so it is accessible for WC 2.0
	 *
	 * @since 1.1
	 *
	 * @param  array $data
	 *
	 * @return string
	 */
	public function get_custom_attribute_html( $data ) {
		$custom_attributes = array();

		if ( ! empty( $data['custom_attributes'] ) && is_array( $data['custom_attributes'] ) ) {
			foreach ( $data['custom_attributes'] as $attribute => $attribute_value ) {
				$custom_attributes[] = esc_attr( $attribute ) . '="' . esc_attr( $attribute_value ) . '"';
			}
		}

		return implode( ' ', $custom_attributes );
	}

	/**
	 * Load the iframe template
	 *
	 * @since 2.0
	 *
	 * @param $location
	 *
	 * @return string
	 */
	public function load_iframe_template( $location ) {
		ob_start();

		$width  = $this->get_iframe_dimension( $this->iframe_width, '700' );
		$height = $this->get_iframe_dimension( $this->iframe_height, '850' );

		WC_Compat_Netbanx::wc_get_template(
			'netbanx/iframe.php',
			array(
				'location' => $location,
				'width'    => $width,
				'height'   => $height,
				'scroll'   => $this->iframe_scroll,
			),
			'',
			WC_Netbanx::plugin_path() . '/templates/'
		);

		return ob_get_clean();
	}

	/**
	 * Are we using iframe?
	 *
	 * @since 2.0
	 *
	 * @return bool
	 */
	public function is_using_iframe() {
		return ( 'yes' == $this->use_iframe );
	}

	/**
	 * Performs checks on the set iframe dimensions and returns the value or the default
	 *
	 * @since 2.0
	 *
	 * @param string $dimension The string dimension in pixels (111) or in percentage (55%)
	 * @param string $default   Default value in pixels only
	 *
	 * @return int|string
	 */
	public function get_iframe_dimension( $dimension, $default = '0' ) {
		if ( substr( $dimension, - 1 ) == '%' ) {
			if ( is_numeric( substr( $dimension, 0, ( strlen( $dimension ) - 1 ) ) ) ) {
				$value = $dimension;
			} else {
				$value = $default;
			}
		} elseif ( is_numeric( $dimension ) ) {
			$value = $dimension;
		} else {
			$value = $default;
		}

		return $value;
	}

	/**
	 * Adds the iframe error notification
	 *
	 * @since 2.0
	 *
	 * @param $error_message
	 */
	public function iframe_error_notification_message( $error_message ) {
		// Any exception is logged and flags a notice
		WC_Netbanx::add_debug_log( 'Iframe error: ' . $error_message );

		$message = sprintf(
			__(
				'Error generating the payment form. Please refresh the page and try again.
			 		If error persists, please contact the administrator. Error message: %s ', 'wc_netbanx'
			),
			$error_message
		);

		echo '<p class="netbanx-iframe-error">' . $message . '</p>';
	}

	/**
	 * Ends execution with failed validation message.
	 *
	 * @since 2.0
	 */
	public function end_by_failed_validation() {
		die( __( 'The request did not pass validation.', 'wc_netbanx' ) );
	}

	/**
	 * The string on Pay page, prompting user to pay with the form below
	 *
	 * @return string|void
	 */
	public function string_pay_with_form_below() {
		return __( 'Please review the order summary below and place your order.', 'wc_netbanx' );
	}
}