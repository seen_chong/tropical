<?php

/**
 * Just a shell class in case there are still responses to be send to the old gateway class.
 *
 * @since 1.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2012 - 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
class WC_Gateway_Netbanx {
	public function __construct() {
		new WC_Netbanx_Gateway();

		add_action( 'woocommerce_api_' . strtolower( get_class( $this ) ), array( $this, 'redirect_response' ) );
	}

	public function redirect_response() {
		do_action( 'woocommerce_api_' . strtolower( 'WC_Netbanx_Gateway' ) );
	}
} //end netbanx class
