<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Handles the Hosted Response operations
 *
 * @since  2.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
class WC_Netbanx_Response_Hosted extends WC_Netbanx_Response {
	/**
	 * Process Hosted API payment response
	 *
	 * @since 1.1
	 * @throws InvalidArgumentException
	 **/
	public function process_response() {
		return $this->lookup_the_response_and_return_the_result();
	}

	/**
	 * Validates and processes the refund response
	 *
	 * @since 2.0
	 *
	 * @param \OptimalPayments\HostedPayment\Refund $response              Response of the refund request
	 * @param WC_Order                              $order
	 * @param double                                $amount                Amount to be refunded.
	 * @param string                                $refund_transaction_id Original Netbanx Transaction Order ID
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function process_refund_response( \OptimalPayments\HostedPayment\Refund $response, WC_Order $order, $amount, $refund_transaction_id ) {
		$this->validate_currency( $response->currencyCode, $order->get_order_currency(), $order );
		$this->validate_amount( $response->amount, $this->format_amount( $amount ), $order );

		if ( isset( $response->confirmationNumber ) && '' != $response->confirmationNumber ) {
			return true;
		} else {
			throw new Exception(
				__(
					'Refund was not approved. Please try again or
					 try manual refund through your Netbanx dashboard.', 'wc_netbanx'
				)
			);
		}
	}
}