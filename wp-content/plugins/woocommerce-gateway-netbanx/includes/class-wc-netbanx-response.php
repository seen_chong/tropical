<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Netbanx Response abstract class.
 *
 * @since  2.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
abstract class WC_Netbanx_Response extends WC_Netbanx_Hosted {

	/**
	 * Follows the main procedure of validating and processing a payment response.
	 *
	 * 1. Look up the order in Netbanx system.
	 * 2. Validate the Look up response against the WC order.
	 * 3. Process the WC order based on the Look up response.
	 *
	 * @since 2.0
	 *
	 * @return \OptimalPayments\HostedPayment\Order The Netbanx Look up response object.
	 */
	public function lookup_the_response_and_return_the_result() {
		// Debug log
		WC_Netbanx::add_debug_log( 'Payment response received. Response POST is: ' . print_r( $_POST, true ) );

		// We got a response for a payment, query back the payment
		// to get all information and ensure everything is correct.
		$response = $this->response_order_lookup( WC_Netbanx::get_field( 'id', $_POST, '' ) );

		// Get the order ID from the lookup(requery) object
		$order = $this->get_wc_order_from_response_object( $response );

		// Validate the response
		$this->validate_payment_response( $order, $response );

		// Process the order based on the response received from Netbanx.
		$this->process_order_based_on_response( $response, $order );
		
		return $response;
	}

	/**
	 * Performs an order look up
	 *
	 * @since 2.0
	 * @param string $id Netbanx order ID
	 *
	 * @return \OptimalPayments\HostedPayment\Order
	 */
	public function response_order_lookup( $id ) {
		$request  = new WC_Netbanx_Request_Hosted( $this->get_gateway() );
		$response = $request->process_order_lookup( $id );

		WC_Netbanx::add_debug_log( 'Payment Lookup response: ' . print_r( $response, true ) );

		return $response;
	}

	/**
	 * Validates a response from Netbanx
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 * @param          $response
	 */
	public function validate_payment_response( WC_Order $order, $response ) {
		// Since this check can only be done at a post request,
		// but there are many more times we can validate a response,
		// we will perform it only when applicable
		if ( null !== WC_Netbanx::get_field( 'order_id', $_POST, null ) ) {
			$this->validate_order_id( WC_Netbanx::get_field( 'order_id', $_POST, '0' ), $response->id );
		}

		$this->validate_currency( $order->get_order_currency(), $response->currencyCode );
		$this->validate_amount( $this->format_amount( $order->get_total() ), $response->totalAmount, $order );
		$this->validate_order_status( $order );
	}

	/**
	 * Gets the WC Order from the Lookup order object.
	 *
	 * Inside the addendumData.
	 *
	 * @since 2.0
	 *
	 * @param object $response
	 *
	 * @return WC_Order
	 * @throws InvalidArgumentException
	 */
	public function get_wc_order_from_response_object( $response ) {
		if ( isset( $response->addendumData ) ) {
			foreach ( $response->addendumData as $value ) {
				if ( 'order_id' == $value->key ) {
					return WC_Compat_Netbanx::wc_get_order( (int) $value->value );
				}
			}
		}
		throw new InvalidArgumentException( __( 'The order ID in the response and the re-query ID do not match.', 'wc_netbanx' ) );
	}

	/**
	 * Adds an order note with most of the transaction information.
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 * @param object   $response
	 */
	public function add_payment_details_note( WC_Order $order, $response ) {

		$update_details = sprintf(
			__(
				'Netbanx Payment Details:
Status: %s.
Transaction Confirmation Number: %s,
Card type: %s,
Last four digits: %s', 'wc_netbanx'
			),
			$response->transaction->status,
			$response->transaction->confirmationNumber,
			$response->transaction->card->brand,
			$response->transaction->card->lastDigits
		);

		WC_Netbanx::add_debug_log( 'Netbanx Payment Details: ' . $update_details );

		// Update order
		$order->add_order_note( $update_details );
	}

	/**
	 * Gets potential error code and message from the payment response
	 *
	 * @since 2.0
	 *
	 * @param object $response
	 *
	 * @return string
	 */
	function get_errors_from_response( $response ) {
		if ( isset( $response->transaction->errorCode ) ) {
			$error_code = ' Error Code: ' . $response->transaction->errorCode;
		}

		if ( isset( $response->transaction->errorMessage ) ) {
			$error_message = ' Error Code: ' . $response->transaction->errorMessage;
		}

		return sprintf( '%s%s', $error_code, $error_message );
	}

	/**
	 * Saves the transaction details to the given order.
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 * @param          $response
	 */
	public function save_transaction_details_to_order( WC_Order $order, $response ) {
		update_post_meta( $order->id, '_netbanx_payment_order_id', $response->id );
		update_post_meta( $order->id, '_netbanx_transaction_payment_type', $response->transaction->card->type );
		update_post_meta( $order->id, '_netbanx_transaction_card_brand', $response->transaction->card->brand );
		update_post_meta( $order->id, '_netbanx_transaction_card_last_four', $response->transaction->card->lastDigits );
		update_post_meta( $order->id, '_netbanx_transaction_card_expiration', $response->transaction->card->expiry );
	}

	/**
	 * Validates the order ID against the WC Order.
	 *
	 * @since 2.0
	 *
	 * @param $order_id
	 * @param $response_id
	 */
	public function validate_order_id( $order_id, $response_id ) {
		// Those IDs should always match
		if ( $order_id == $response_id ) {
			throw new InvalidArgumentException( __( 'The order ID in the response and security check order ID do not match.', 'wc_netbanx' ) );
		}
	}

	/**
	 * Validates the currency against the store currency.
	 *
	 * @since 2.0
	 *
	 * @param string   $order_currency
	 * @param string   $response_currency
	 * @param WC_Order $order
	 */
	public function validate_currency( $order_currency, $response_currency, $order ) {
		// Check order currency
		if ( $order_currency != $response_currency ) {
			$message = __( 'Currency of the payment did not match the currency the order.', 'wc_netbanx' );
			$order->add_order_note( $message );

			throw new InvalidArgumentException( $message );
		}
	}

	/**
	 * Validates the amount paid against the store order amount.
	 *
	 * @since 2.0
	 *
	 * @param $order_total
	 * @param $response_total
	 * @param $order
	 */
	public function validate_amount( $order_total, $response_total, $order ) {
		// Check the amount
		if ( $order_total != $response_total ) {
			$message = __( 'Response amount did not match the given order amount.', 'wc_netbanx' );
			$order->add_order_note( $message );

			throw new InvalidArgumentException( $message );
		}
	}

	/**
	 * Checks that the order is not already processed(Processing or Completed status).
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 */
	public function validate_order_status( $order ) {
		// Check if order was processed already
		if ( 'complete' == WC_Compat_Netbanx::get_order_status( $order ) || 'processing' == WC_Compat_Netbanx::get_order_status( $order ) ) {
			$message = __( 'Netbanx: Received response, but order was already paid for.', 'wc_netbanx' );
			$order->add_order_note( $message );

			throw new InvalidArgumentException( $message );
		}
	}

	/**
	 * Processes the order based on the payment status.
	 * Will complete or fail an order based on the payment status.
	 * Adds debug logs and the appropriate order notes.
	 *
	 * @since 2.0
	 *
	 * @param $response
	 * @param $order
	 */
	public function process_order_based_on_response( $response, $order ) {
		// Check the payment status
		switch ( $response->transaction->status ) :
			case 'success' :
				$this->process_response_status_success( $order, $response );
				break;
			case 'declined' :
				$this->process_response_status_declined( $response, $order );
				break;
			case 'abandoned' :
				$this->process_response_status_abandoned( $response, $order );
				break;
			case 'held' :
				$this->process_response_status_held( $response, $order );
				break;
			case 'cancelled' :
				$this->process_response_status_cancelled( $order );
				break;
			default :
				$this->process_response_status_default( $response, $order );
				break;
		endswitch;
	}

	/**
	 * Processes the order for successful payment
	 *
	 * @since 2.0
	 *
	 * @param $order
	 * @param $response
	 */
	public function process_response_status_success( $order, $response ) {
		$this->add_payment_details_note( $order, $response );

		// Add transaction details to order
		$this->save_transaction_details_to_order( $order, $response );

		// Debug log
		WC_Netbanx::add_debug_log( 'Payment completed.' );

		WC_Compat_Netbanx::payment_complete( $order, $response->id );

		// Save the payment profile to the customer meta
		$this->save_customer_payment_profile( $order, $response );
	}

	/**
	 * Processes the order for declined payment
	 *
	 * @since 2.0
	 *
	 * @param $response
	 * @param $order
	 */
	public function process_response_status_declined( $response, $order ) {
		$error_details = $this->get_errors_from_response( $response );

		// Debug log
		WC_Netbanx::add_debug_log( sprintf( 'Payment declined. %s', $error_details ) );

		$order->update_status( 'failed' );

		// Update order
		$order->add_order_note( sprintf( __( 'Netbanx Payment Declined.', 'wc_netbanx' ) ) );
	}

	/**
	 * Processes the order for abandoned payment
	 *
	 * @since 2.0
	 *
	 * @param $response
	 * @param $order
	 */
	public function process_response_status_abandoned( $response, $order ) {
		$error_details = $this->get_errors_from_response( $response );

		// Debug log
		WC_Netbanx::add_debug_log( sprintf( 'Payment was abandoned part way through. %s', $error_details ) );

		$order->update_status( 'failed' );

		// Update order
		$order->add_order_note( sprintf( __( 'Payment was abandoned part way through.', 'wc_netbanx' ) ) );
	}

	/**
	 * Processes the order for held payment.
	 *
	 * @since 2.0
	 *
	 * @param $response
	 * @param $order
	 */
	public function process_response_status_held( $response, $order ) {
		// Debug log
		WC_Netbanx::add_debug_log(
			sprintf(
				'Transaction has been placed on hold due to risk rules results.
						Risk Reason Code: %s.',
				$response->transaction->riskReasonCode
			)
		);

		$order->update_status( 'on-hold' );

		// Update order
		$order->add_order_note(
			sprintf(
				__(
					'Transaction has been placed on hold due to risk rules results.
							Risk Reason Code: %s.', 'wc_netbanx'
				),
				$response->transaction->riskReasonCode
			)
		);

		// Add transaction details to order
		$this->save_transaction_details_to_order( $order, $response );

		// Save the payment profile to the customer meta
		$this->save_customer_payment_profile( $order, $response );
	}

	/**
	 * Processes the order for cancelled payment.
	 *
	 * @since 2.0
	 *
	 * @param $order
	 */
	public function process_response_status_cancelled( $order ) {
		// Debug log
		WC_Netbanx::add_debug_log( 'Transaction has been cancelled.' );

		$order->update_status( 'cancelled' );

		$order->add_order_note( __( 'Transaction has been cancelled.', 'wc_netbanx' ) );
	}

	/**
	 * Processes the order for default payment status(any status not already handled).
	 *
	 * @since 2.0
	 *
	 * @param $response
	 * @param $order
	 */
	public function process_response_status_default( $response, $order ) {
		$error_details = $this->get_errors_from_response( $response );

		// Debug log
		WC_Netbanx::add_debug_log( sprintf( 'Unrecognized payment status was received.%s', $error_details ) );

		$order->update_status( 'failed' );

		$order->add_order_note( __( 'Unrecognized payment status was received.', 'wc_netbanx' ) );
	}
}