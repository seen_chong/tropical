<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Description
 *
 * @since  2.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
class WC_Netbanx_Gateway_Addons_Deprecated extends WC_Netbanx_Gateway_Addons {
	public function __construct() {
		parent::__construct();

		// Add support for Subscriptions
		if ( 1 === WC_Netbanx::get_subscriptions_version() ) {
			// Subscriptions Actions
			add_action( 'scheduled_subscription_payment_' . $this->id, array( $this, 'scheduled_subscription_payment_request' ), 10, 3 );
			add_filter( 'woocommerce_subscriptions_renewal_order_meta_query', array( $this, 'remove_renewal_order_meta' ), 10, 4 );
			add_action( 'woocommerce_subscriptions_changed_failing_payment_method_' . $this->id, array( $this, 'changed_failing_payment_method' ), 10, 2 );
			add_filter( 'woocommerce_my_subscriptions_recurring_payment_method', array( $this, 'maybe_render_subscription_payment_method' ), 10, 3 );
		}

		// Actions
		add_action( 'woocommerce_api_' . strtolower( get_class( $this ) ), array( $this, 'process_response' ) );
	}

	/**
	 * Process the payment
	 *
	 * @param int $order_id
	 *
	 * @return array|type|void
	 */
	function process_payment( $order_id ) {
		$order = WC_Compat_Netbanx::wc_get_order( $order_id );
		if ( $this->order_contains_subscription( $order ) ) {
			// Subscription 1.n Payment
			return $this->process_subscription( $order );
		} else {
			return parent::process_payment( $order_id );
		}
	}


	/**
	 * Call the iframe generation method
	 *
	 * @param int $order_id
	 */
	function receipt_page( $order_id ) {
		$order = WC_Compat_Netbanx::wc_get_order( $order_id );
		if ( $this->order_contains_subscription( $order ) ) {
			try {
				echo '<p>' . $this->string_pay_with_form_below() . '</p>';

				echo $this->generate_netbanx_form_subscriptions_deprecated( $order );
			}
			catch ( Exception $ex ) {
				echo $ex->getMessage();
			}
		} else {
			parent::receipt_page( $order_id );
		}
	}

	/**
	 * Check the payment response and process the order
	 */
	public function process_response() {
		try {
			$api_response = new WC_Netbanx_Response_Hosted_Addons_Deprecated( $this );
			$api_response->process_response();

			WC_Compat_Netbanx::empty_cart();
		}
		catch ( Exception $ex ) {
			// Debug log
			WC_Netbanx::add_debug_log( $ex->getMessage() );

			$this->end_by_failed_validation();
		}

		// Return 204 status
		header( "HTTP/1.0 204 No Content" );
		exit;
	}

	/**
	 * Process subscription payment
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return type
	 */
	public function process_subscription( WC_Order $order ) {
		try {
			if ( $this->is_using_iframe() ) {
				$url = $order->get_checkout_payment_url( true );
			} else {
				$request = new WC_Netbanx_Request_Hosted_Subscriptions_Deprecated( $this );
				$url     = $request->get_payment_url( $order );
			}

			ob_clean();
			return array(
				'result'   => 'success',
				'redirect' => $url
			);
		}
		catch ( Exception $e ) {
			// Any exception is logged and flags a notice
			WC_Netbanx::add_debug_log( 'Initial Subscription payment response error: ' . $e->getMessage() );

			WC_Compat_Netbanx::wc_add_notice( $e->getMessage(), 'error' );
		}
	}

	/**
	 * Display the Netbanx payment form iframe
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function generate_netbanx_form_subscriptions_deprecated( WC_Order $order ) {
		try {
			$request  = new WC_Netbanx_Request_Hosted_Subscriptions_Deprecated( $this );
			$location = $request->get_payment_url( $order );

			return $this->load_iframe_template( $location );
		}
		catch ( Exception $e ) {
			$this->iframe_error_notification_message( $e->getMessage() );
		}
	}

	/**
	 * Process subscription payment for each period
	 *
	 * @since  2.0
	 *
	 * @param float    $amount_to_charge The amount to charge.
	 * @param WC_Order $order            The WC_Order object of the order which the subscription was purchased in.
	 * @param int      $product_id       The ID of the subscription product for which this payment relates.
	 *
	 * @access public
	 * @return void
	 */
	public function scheduled_subscription_payment_request( $amount_to_charge, \WC_Order $renewal_order, $product_id ) {
		try {
			WC_Netbanx::add_debug_log( 'Processing Scheduled Subscription Payment. Order: ' . $renewal_order->id );

			// Process a rebill request
			$api_request = new WC_Netbanx_Request_Hosted_Subscriptions_Deprecated( $this );
			$rebill      = $api_request->process_subscription_rebill( $renewal_order, $amount_to_charge );

			// Process the order from the response
			$api_response = new WC_Netbanx_Response_Hosted_Addons_Deprecated( $this );
			$api_response->process_scheduled_payment_response( $rebill, $renewal_order, $product_id );
		}
		catch ( Exception $e ) {
			$renewal_order->update_status( 'failed', $e->getMessage() );

			// Debug log
			WC_Netbanx::add_debug_log( $e->getMessage() );

			WC_Subscriptions_Manager::process_subscription_payment_failure_on_order( $renewal_order, $product_id );
		}
	}

	/**
	 * Don't transfer Netbanx customer/token meta when creating a parent renewal order.
	 *
	 * @since 2.0
	 *
	 * @param array  $order_meta_query  MySQL query for pulling the metadata
	 * @param int    $original_order_id Post ID of the order being used to purchased the subscription being renewed
	 * @param int    $renewal_order_id  Post ID of the order created for renewing the subscription
	 * @param string $new_order_role    The role the renewal order is taking, one of 'parent' or 'child'
	 *
	 * @return void
	 */
	public function remove_renewal_order_meta( $order_meta_query, $original_order_id, $renewal_order_id, $new_order_role ) {
		if ( 'parent' == $new_order_role ) {
			$order_meta_query .= " AND `meta_key` NOT LIKE '_netbanx_payment_order_id' ";
			$order_meta_query .= " AND `meta_key` NOT LIKE '_netbanx_transaction_payment_type' ";
			$order_meta_query .= " AND `meta_key` NOT LIKE '_netbanx_transaction_card_brand' ";
			$order_meta_query .= " AND `meta_key` NOT LIKE '_netbanx_transaction_card_last_four' ";
			$order_meta_query .= " AND `meta_key` NOT LIKE '_netbanx_transaction_card_expiration' ";
		}

		return $order_meta_query;
	}

	/**
	 * Update the accountID and the Serial No on the original order, when a renewal order is placed, after it failed previously
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $original_order
	 * @param WC_Order $renewal_order
	 */
	public function changed_failing_payment_method( \WC_Order $original_order, \WC_Order $renewal_order ) {
		update_post_meta( $original_order->id, '_netbanx_payment_order_id', get_post_meta( $renewal_order->id, '_netbanx_payment_order_id', true ) );
	}


	/**
	 * Show payment method on My Subscriptions section of My Account page
	 *
	 * @since 2.0
	 *
	 * @param string   $payment_method_to_display
	 * @param array    $subscription_details
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function maybe_render_subscription_payment_method( $payment_method_to_display, $subscription_details, \WC_Order $order ) {

		if ( $this->id !== $order->recurring_payment_method ) {
			return $payment_method_to_display;
		}

		$last4      = get_post_meta( $order->id, '_netbanx_transaction_card_last_four', true );
		$card_type  = get_post_meta( $order->id, '_netbanx_transaction_payment_type', true );
		$card_brand = get_post_meta( $order->id, '_netbanx_transaction_card_brand', true );

		if ( '' != $last4 && ( '' != $card_type || '' != $card_brand ) ) {
			$payment_method_to_display = sprintf( __( 'Via %s card ending in %s', 'wc_netbanx' ), '' != $card_brand ? $card_brand : $card_type, $last4 );
		}

		return $payment_method_to_display;
	}

	/**
	 * Returns true, if order contains subscription
	 *
	 * @param WC_Order $order
	 *
	 * @return bool
	 */
	public function order_contains_subscription( WC_Order $order ) {
		if (
			1 === WC_Netbanx::get_subscriptions_version()
			&& WC_Subscriptions_Order::order_contains_subscription( $order )
		) {
			return true;
		}

		return false;
	}
}