<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Handles the Hosted response for Subscriptions 1.n
 *
 * @since  2.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
class WC_Netbanx_Response_Hosted_Addons_Deprecated extends WC_Netbanx_Response {
	/**
	 * Process Hosted API payment response
	 *
	 * @since 2.0
	 * @throws InvalidArgumentException
	 **/
	function process_response() {
		$response = $this->lookup_the_response_and_return_the_result();
		$order = $this->get_wc_order_from_response_object( $response );

		if ( 'success' == $response->transaction->status ) {
			// Activate the subscriptions
			WC_Subscriptions_Manager::activate_subscriptions_for_order( $order );
		}
	}

	/**
	 * Process scheduled payment response
	 *
	 * @since 2.0
	 *
	 * @param          $response
	 * @param WC_Order $order
	 * @param          $product_id
	 */
	public function process_scheduled_payment_response( $response, WC_Order $order, $product_id ) {
		$this->process_order_based_on_response( $response, $order );

		// If status is success, process the renewal
		if ( 'success' == $response->transaction->status ) {
			// Activate the subscriptions
			WC_Subscriptions_Manager::process_subscription_payments_on_order( $order );
		} else {
			// If status is anything else, fail the renewal
			WC_Subscriptions_Manager::process_subscription_payment_failure_on_order( $order, $product_id );
		}
	}
}