<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Main API class. Contains general tasks used by both Requests and Response processes.
 *
 * @since  2.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
abstract class WC_Netbanx_Hosted {

	private $gateway = null;

	private $client = null;

	/**
	 * Constructor of the class
	 *
	 * @since 2.0
	 *
	 * @param WC_Payment_Gateway $gateway
	 */
	public function __construct( WC_Payment_Gateway $gateway ) {
		$this->set_gateway( $gateway );
	}

	/**
	 * Sets the gateway class to a class variable
	 *
	 * @since 2.0
	 *
	 * @param WC_Payment_Gateway $gateway
	 */
	private function set_gateway( WC_Payment_Gateway $gateway ) {
		$this->gateway = $gateway;
	}

	/**
	 * Returns the variable with the gateway class
	 *
	 * @since 2.0
	 *
	 * @return WC_Payment_Gateway
	 * @throws Exception
	 */
	public function get_gateway() {
		if ( null == $this->gateway ) {
			throw new InvalidArgumentException( __( 'Gateway class not initiated.', 'wc_netbanx' ) );
		}

		return $this->gateway;
	}

	/**
	 * Returns the initiated OptimalApiClient class.
	 *
	 * Double checks that the OptimalApiClient class is initiated and initiates it, if it is not.
	 *
	 * @since 2.0
	 *
	 * @return OptimalPayments\OptimalApiClient
	 */
	public function get_client() {
		if ( null == $this->client ) {
			$this->client = $this->set_client();
		}

		return $this->client;
	}

	/**
	 * Loads the SDK and initiates the OptimalApiClient class
	 *
	 * @since 2.0
	 *
	 * @return OptimalPayments\OptimalApiClient
	 * @throws Exception
	 */
	private function set_client() {
		if ( ! class_exists( 'OptimalPayments\\OptimalApiClient' ) ) {
			include_once WC_Netbanx::plugin_path() . '/vendor/optimal-php-sdk/optimalpayments.php';
		}

		return new OptimalPayments\OptimalApiClient(
			$this->get_api_key( $this->get_gateway()->get_option( 'api_key' ) ),
			$this->get_api_secret( $this->get_gateway()->get_option( 'api_key' ) ),
			$this->get_request_environment()
		);
	}

	/**
	 * Returns the API Key from the given API String
	 *
	 * @since 2.0
	 *
	 * @param string $api_string
	 *
	 * @return mixed
	 */
	private function get_api_key( $api_string ) {
		$auth_credentials = explode( ':', $api_string );

		return $auth_credentials[0];
	}

	/**
	 * Returns the API Secret from the given API String
	 *
	 * @since 2.0
	 *
	 * @param string $api_string
	 *
	 * @return mixed
	 */
	private function get_api_secret( $api_string ) {
		$auth_credentials = explode( ':', $api_string );

		return $auth_credentials[1];
	}

	/**
	 * Returns the Netbanx environment based on the testmode plugin setting
	 *
	 * @return mixed
	 */
	private function get_request_environment() {
		if ( 'yes' == $this->get_gateway()->get_option( 'testmode' ) ) {
			$environment = OptimalPayments\Environment::TEST;
		} else {
			$environment = OptimalPayments\Environment::LIVE;
		}

		return $environment;
	}

	/**
	 * Save a payment profile to the customer meta data
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 * @param object   $response
	 *
	 * @return void
	 */
	public function save_customer_payment_profile( WC_Order $order, $response ) {
		if ( ! isset( $response->profile ) ) {
			return;
		}

		// Always save the profile info to the order,
		// as a profile can have multiple cards and the token(associated card)
		// can be important for processing rebills
		$this->save_profile_to_order( $order, $response );

		// Don't save profiles for guest customers
		if ( '' != $order->user_id || 0 != $order->user_id ) {
			$this->save_profile_to_user( $order, $response );
		}
	}

	/**
	 * Save profile data to the order meta
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 * @param          $response
	 */
	private function save_profile_to_order( WC_Order $order, $response ) {
		// Get any existing profile values
		$netbanx_profile = $this->get_order_profile_id( $order->id );
		$netbanx_token   = $this->get_order_profile_token( $order->id );

		WC_Netbanx::add_debug_log( 'Saving to Order.' );

		// Update the profile id, if needed
		if ( isset( $response->profile->id ) && $netbanx_profile != $response->profile->id ) {
			WC_Netbanx::add_debug_log( 'Saved ID: ' . $response->profile->id );
			update_post_meta( $order->id, $this->get_order_profile_id_field_name(), $response->profile->id );
		}

		// Update token if it's present and different then the already existing
		if ( isset( $response->profile->paymentToken ) && $netbanx_token != $response->profile->paymentToken ) {
			WC_Netbanx::add_debug_log( 'Saved Token: ' . $response->profile->paymentToken );
			update_post_meta( $order->id, $this->get_order_profile_token_field_name(), $response->profile->paymentToken );
		}
	}

	/**
	 * Save profile data to the user meta
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 * @param          $response
	 */
	private function save_profile_to_user( WC_Order $order, $response ) {
		// Get any existing profile values
		$netbanx_profile = $this->get_user_profile_id( $order->user_id );
		$netbanx_token   = $this->get_user_profile_token( $order->user_id );

		WC_Netbanx::add_debug_log( 'Saving to User.' );

		// Update the profile id, if needed
		if ( isset( $response->profile->id ) && $netbanx_profile != $response->profile->id ) {
			WC_Netbanx::add_debug_log( 'Saved ID: ' . $response->profile->id );
			update_user_meta( $order->user_id, $this->get_user_profile_id_field_name(), $response->profile->id );
		}

		// Update token if it's present and different then the already existing
		if ( isset( $response->profile->paymentToken ) && $netbanx_token != $response->profile->paymentToken ) {
			WC_Netbanx::add_debug_log( 'Saved Token: ' . $response->profile->paymentToken );
			update_user_meta( $order->user_id, $this->get_user_profile_token_field_name(), $response->profile->paymentToken );
		}
	}

	/**
	 * Returns profile ID saved to the user meta
	 *
	 * @since 2.0
	 *
	 * @param int $user_id
	 *
	 * @return mixed
	 */
	public function get_user_profile_id( $user_id ) {
		return get_user_meta( $user_id, $this->get_user_profile_id_field_name(), true );
	}

	/**
	 * Returns profile token saved to the user meta
	 *
	 * @since 2.0
	 *
	 * @param int $user_id
	 *
	 * @return mixed
	 */
	public function get_user_profile_token( $user_id ) {
		return get_user_meta( $user_id, $this->get_user_profile_token_field_name(), true );
	}

	/**
	 * Returns profile ID saved to the order meta
	 *
	 * @since 2.0
	 *
	 * @param int $order_id
	 *
	 * @return mixed
	 */
	public function get_order_profile_id( $order_id ) {
		return get_post_meta( $order_id, $this->get_order_profile_id_field_name(), true );
	}

	/**
	 * Returns profile token saved to the order meta
	 *
	 * @since 2.0
	 *
	 * @param int $order_id
	 *
	 * @return mixed
	 */
	public function get_order_profile_token( $order_id ) {
		return get_post_meta( $order_id, $this->get_order_profile_token_field_name(), true );
	}

	/**
	 * Returns order profile ID meta field name
	 *
	 * @since 2.0
	 * @return string
	 */
	private function get_order_profile_id_field_name() {
		return '_netbanx_hosted_order_profile_id';
	}

	/**
	 * Returns order profile token meta field name
	 *
	 * @since 2.0
	 * @return string
	 */
	private function get_order_profile_token_field_name() {
		return '_netbanx_hosted_order_profile_token';
	}

	/**
	 * Returns user profile ID meta field name
	 *
	 * @since 2.0
	 * @return string
	 */
	private function get_user_profile_id_field_name() {
		return '_netbanx_hosted_customer_profile_id';
	}

	/**
	 * Returns user profile token meta field name
	 *
	 * @since 2.0
	 * @return string
	 */
	private function get_user_profile_token_field_name() {
		return '_netbanx_hosted_customer_profile_token';
	}

	/**
	 * Format amount for requests. Amount should be with no decimals and no leading 0.
	 *
	 * @since 2.0
	 *
	 * @param $amount
	 *
	 * @return string
	 */
	public function format_amount( $amount ) {
		$formatted = ltrim( number_format( $amount, 2, '', '' ), '0' );

		// Since we are trimming 0 we can end up with an empty string on free orders
		// so in this case make sure amount is 0.
		if ( '' == $formatted ) {
			$formatted = 0;
		}

		return $formatted;
	}

	/**
	 * Convert string to UTF-8
	 *
	 * @since 2.0
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public function convert_to_utf( $str ) {
		return mb_convert_encoding( $str, 'utf-8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS,windows-1251' );
	}

	/**
	 * Returns the Netbanx Response object ID also referred as Transaction ID.
	 *
	 * This ID can be used to run rebill transactions.
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return mixed
	 */
	public function get_netbanx_payment_order_id( WC_Order $order ) {
		return get_post_meta( $order->id, '_netbanx_payment_order_id', true );
	}

	/**
	 * Formats and returns a the passed string
	 *
	 * @since 2.0
	 *
	 * @param string $string String to be formatted
	 * @param int $limit Limit characters of the string
	 *
	 * @return string
	 */
	public function format_string( $string, $limit ) {
		if ( strlen( $string ) > $limit ) {
			$string = substr( $string, 0, ( $limit - 3 ) ) . '...';
		}

	    	return html_entity_decode( $this->convert_to_utf( $string ), ENT_NOQUOTES, 'UTF-8' );
	}
}