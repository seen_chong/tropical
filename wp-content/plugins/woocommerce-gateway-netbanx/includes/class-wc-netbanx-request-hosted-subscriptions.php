<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Description
 *
 * @since  2.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
class WC_Netbanx_Request_Hosted_Subscriptions extends WC_Netbanx_Request {
	/**
	 * Returns a payment request URL link.
	 *
	 * It initialize the process of payment request.
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function get_payment_url( WC_Order $order ) {
		$params = $this->build_payment_request( $order );

		// Allow for parameters modification
		$params = apply_filters( 'wc_netbanx_addons_request_params', $params, $order );

		$response = $this->attempt_to_process_order( $order, $params );

		return $response->getLink( 'hosted_payment' )->uri;
	}

	/**
	 * Setup the Netbanx payment request
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function build_payment_request( WC_Order $order ) {
		// Debug log
		WC_Netbanx::add_debug_log( 'Generating Subscription payment form for order #' . $order->id );

		$amount = $this->format_amount( $order->get_total() );

		$request_params = array(
			'merchantRefNum'            => WC_Netbanx::get_order_number( $order ) . '_' . $this->get_attempts_suffix( $order ),
			'totalAmount'               => $amount,
			'currencyCode'              => WC_Compat_Netbanx::get_order_currency( $order ),
			'customerNotificationEmail' => $order->billing_email,
			'customerIp'                => $this->get_user_ip_addr(),
			'locale'                    => $this->get_locale( $order ),
			'billingDetails'            => $this->get_billing_fields( $order ),
			'callback'                  => $this->get_callback_fields(),
			'addendumData'              => $this->get_addendum_data_fields( $order ),
			'link'                      => $this->get_link_fields( $order ),
		);

		// Add shipping fields
		if ( '' !== $order->shipping_address_1 ) {
			$request_params['shippingDetails'] = $this->get_shipping_fields( $order );
		}

		// Add order details
		if ( $this->maybe_send_order_details() ) {
			if ( 0 == $amount ) {
				$request_params['shoppingCart'] = $this->get_free_subscription_shopping_cart_fields( $order );
			} else {
				$request_params['shoppingCart'] = $this->get_shopping_cart_fields( $order, 'including' );
			}
		}

		// If we don't have to charge a payment. Total 0
		if ( 0 == $amount ) {
			$request_params['extendedOptions'][] = array(
				'key'   => 'authType',
				'value' => 'auth',
			);
		}

		// Add the customer profile node
		$request_params['profile'] = $this->add_customer_profile_fields( $order );

		return $request_params;
	}

	/**
	 * @param WC_Order $order
	 * @param double   $amount
	 *
	 * @return OptimalPayments\HostedPayment\Order
	 */
	public function process_subscription_rebill( WC_Order $order, $amount = null ) {
		$transaction_id = $this->get_netbanx_payment_order_id( $order );

		// Amount is the order total, if not given any different
		if ( null === $amount ) {
			$amount = $order->get_total();
		}

		$params = array(
			'id'             => $transaction_id,
			'totalAmount'    => $this->format_amount( $amount ),
			'currencyCode'   => $order->get_order_currency(),
			'merchantRefNum' => WC_Netbanx::get_order_number( $order ) . '_' . $this->get_attempts_suffix( $order ),
		);

		$params['shoppingCart'][] = array(
			'amount'      => $this->format_amount( $amount ),
			'description' => $this->format_string( sprintf( __( '%s - Order %s', 'wc_netbanx' ), wp_specialchars_decode( get_bloginfo( 'name' ), ENT_QUOTES ), $order->get_order_number() ), 50 ),
			'quantity'    => 1,
		);

		return $this->process_rebill( $params );
	}

	/**
	 * Free Subscription order description
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return array
	 */
	private function get_free_subscription_shopping_cart_fields( WC_Order $order ) {
		return array(
			array(
				'amount'      => 0,
				'description' => $this->format_string(
					sprintf(
						__( 'Payment for Order %s.', 'wc_netbanx' ),
						WC_Netbanx::get_order_number( $order )
					),
					50
				),
				'quantity'    => 1,
			)
		);
	}
}