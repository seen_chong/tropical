<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Description
 *
 * @since  2.0
 * @author VanboDevelops | Ivan Andreev
 *
 *        Copyright: (c) 2015 VanboDevelops
 *        License: GNU General Public License v3.0
 *        License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
class WC_Netbanx_Gateway_Addons extends WC_Netbanx_Gateway {

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();

		if ( 2 === WC_Netbanx::get_subscriptions_version() ) {
			// Scheduled payment
			add_action( 'woocommerce_scheduled_subscription_payment_' . $this->id, array( $this, 'scheduled_subscription_payment_request' ), 10, 2 );

			// Meta data renewal remove
			add_action( 'wcs_resubscribe_order_created', array( $this, 'remove_renewal_order_meta' ), 10 );

			// Update change payment method
			add_action( 'woocommerce_subscription_failing_payment_method_updated_' . $this->id, array( $this, 'changed_failing_payment_method' ), 10, 2 );

			// Display card used details
			add_filter( 'woocommerce_my_subscriptions_payment_method', array( $this, 'maybe_render_subscription_payment_method' ), 10, 2 );

			// Handle display of the Admin facing payment method change
			add_filter( 'woocommerce_subscription_payment_meta', array( $this, 'add_subscription_payment_meta' ), 10, 2 );
			// Handle validation of the Admin facing payment method change
			add_filter( 'woocommerce_subscription_validate_payment_meta', array( $this, 'validate_subscription_payment_meta' ), 10, 2 );
		}

		// Add support for Pre-Orders
		if ( WC_Netbanx::is_pre_orders_active() ) {
			add_action( 'wc_pre_orders_process_pre_order_completion_payment_' . $this->id, array( $this, 'process_pre_order_release_payment' ) );
		}

		// Actions
		add_action( 'woocommerce_api_' . strtolower( get_class( $this ) ), array( $this, 'process_response' ) );
	}

	/**
	 * Process the payment
	 *
	 * @since 2.0
	 *
	 * @param  int $order_id
	 *
	 * @return array
	 */
	public function process_payment( $order_id ) {
		$order = WC_Compat_Netbanx::wc_get_order( $order_id );
		if (
			$this->order_contains_subscription( $order )
			|| $this->order_contains_pre_order( $order )
		) {
			// Processing subscription
			return $this->process_order_payment( $order );
		} else {
			return parent::process_payment( $order_id );
		}
	}

	/**
	 * Process the subscription
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return array
	 */
	public function process_order_payment( WC_Order $order ) {
		try {
			if ( $this->is_using_iframe() ) {
				// To Pay Page
				$url = $order->get_checkout_payment_url( true );
			} elseif ( $this->order_contains_pre_order( $order ) ) {
				// Pre Order
				$url = $this->get_pre_orders_payment_url( $order );
			} elseif ( $this->order_contains_subscription( $order ) ) {
				// Subscription
				$url = $this->get_subscriptions_payment_url( $order );
			}

			ob_clean();

			return array(
				'result'   => 'success',
				'redirect' => $url
			);
		}
		catch ( Exception $e ) {
			// Any exception is logged and flags a notice
			WC_Netbanx::add_debug_log( 'Addons payment response error: ' . $e->getMessage() );

			WC_Compat_Netbanx::wc_add_notice( $e->getMessage(), 'error' );
		}
	}

	/**
	 * Call the iframe generation method
	 *
	 * @since 2.0
	 *
	 * @param int $order_id
	 */
	public function receipt_page( $order_id ) {
		$order = WC_Compat_Netbanx::wc_get_order( $order_id );

		if ( $this->order_contains_subscription( $order )
			|| $this->order_contains_pre_order( $order )
		) {
			$this->output_iframe_on_pay_page( $order );
		} else {
			parent::receipt_page( $order_id );
		}
	}

	/**
	 * Generates and displays the iframe on the pay page
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 */
	public function output_iframe_on_pay_page( WC_Order $order ) {
		try {
			echo '<p>' . $this->string_pay_with_form_below() . '</p>';

			if ( $this->order_contains_subscription( $order ) ) {
				echo $this->generate_netbanx_iframe_subscriptions( $order );
			} elseif ( $this->order_contains_pre_order( $order ) ) {
				echo $this->generate_netbanx_iframe_pre_orders( $order );
			}
		}
		catch ( Exception $ex ) {
			echo $ex->getMessage();
		}
	}

	/**
	 * Display the Netbanx payment form iframe
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function generate_netbanx_iframe_subscriptions( WC_Order $order ) {
		try {
			$location = $this->get_subscriptions_payment_url( $order );

			return $this->load_iframe_template( $location );
		}
		catch ( Exception $e ) {
			$this->iframe_error_notification_message( $e->getMessage() );
		}
	}

	/**
	 * Display the Netbanx payment form iframe
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function generate_netbanx_iframe_pre_orders( WC_Order $order ) {
		try {
			$location = $this->get_pre_orders_payment_url( $order );

			return $this->load_iframe_template( $location );
		}
		catch ( Exception $e ) {
			$this->iframe_error_notification_message( $e->getMessage() );
		}
	}

	/**
	 * Don't transfer Netbanx meta to resubscribe orders.
	 *
	 * @since 2.0
	 *
	 * @param int $resubscribe_order The order created for the customer to resubscribe to the old expired/cancelled subscription
	 *
	 * @return void
	 */
	public function remove_renewal_order_meta( $resubscribe_order ) {
		delete_post_meta( $resubscribe_order->id, '_netbanx_payment_order_id' );
		delete_post_meta( $resubscribe_order->id, '_netbanx_transaction_payment_type' );
		delete_post_meta( $resubscribe_order->id, '_netbanx_transaction_card_brand' );
		delete_post_meta( $resubscribe_order->id, '_netbanx_transaction_card_last_four' );
		delete_post_meta( $resubscribe_order->id, '_netbanx_transaction_card_expiration' );
	}

	/**
	 * Perform a subscription scheduled payment
	 *
	 * @since 2.0
	 *
	 * @param $amount_to_charge
	 * @param $renewal_order
	 */
	public function scheduled_subscription_payment_request( $amount_to_charge, $renewal_order ) {
		try {
			WC_Netbanx::add_debug_log( 'Scheduled payment: ' . print_r( $renewal_order->id, true ) );

			// Process a rebill request
			$request = new WC_Netbanx_Request_Hosted_Subscriptions( $this );
			$rebill  = $request->process_subscription_rebill( $renewal_order, $amount_to_charge );

			// Process the order from the response
			$response = new WC_Netbanx_Response_Hosted_Addons( $this );
			$response->process_order_based_on_response( $rebill, $renewal_order );;
		}
		catch ( Exception $e ) {
			$renewal_order->update_status( 'failed', $e->getMessage() );

			// Debug log
			WC_Netbanx::add_debug_log( $e->getMessage() );
		}
	}

	/**
	 * Check the payment response and process the order
	 *
	 * @since 2.0
	 */
	public function process_response() {
		try {
			$response = new WC_Netbanx_Response_Hosted_Addons( $this );
			$response->process_response();

			WC_Compat_Netbanx::empty_cart();
		}
		catch ( Exception $ex ) {
			// Debug log
			WC_Netbanx::add_debug_log( $ex->getMessage() );

			$this->end_by_failed_validation();
		}

		// Return 204 status
		header( "HTTP/1.0 204 No Content" );
		exit;
	}

	/**
	 * Add the Transaction ID to a changed failing payment method
	 *
	 * @since 2.0
	 *
	 * @param $subscription
	 * @param $renewal_order
	 */
	public function changed_failing_payment_method( $subscription, $renewal_order ) {
		update_post_meta( $subscription->id, '_netbanx_payment_order_id', get_post_meta( $renewal_order->id, '_netbanx_payment_order_id', true ) );
	}

	/**
	 * Display the payment method info to the customer.
	 *
	 * @since 2.0
	 *
	 * @param $payment_method_to_display
	 * @param $subscription
	 *
	 * @return string
	 */
	public function maybe_render_subscription_payment_method( $payment_method_to_display, $subscription ) {
		if ( $this->id !== $subscription->payment_method ) {
			return $payment_method_to_display;
		}

		$last4      = get_post_meta( $subscription->id, '_netbanx_transaction_card_last_four', true );
		$card_type  = get_post_meta( $subscription->id, '_netbanx_transaction_payment_type', true );
		$card_brand = get_post_meta( $subscription->id, '_netbanx_transaction_card_brand', true );

		// If could not find any card info on the subscription try the order.
		if ( '' == $last4 ) {
			$last4      = get_post_meta( $subscription->order->id, '_netbanx_transaction_card_last_four', true );
			$card_type  = get_post_meta( $subscription->order->id, '_netbanx_transaction_payment_type', true );
			$card_brand = get_post_meta( $subscription->order->id, '_netbanx_transaction_card_brand', true );
		}

		// If we found at least the last four digits and either the type or brand, display the data.
		if ( '' != $last4 && ( '' != $card_type || '' != $card_brand ) ) {
			$payment_method_to_display = sprintf( __( 'Via %s card ending in %s', 'wc_netbanx' ), '' != $card_brand ? $card_brand : $card_type, $last4 );
		}

		return $payment_method_to_display;
	}

	/**
	 * Add payment method change fields
	 *
	 * @since 2.0
	 *
	 * @param $payment_meta
	 * @param $subscription
	 *
	 * @return mixed
	 */
	public function add_subscription_payment_meta( $payment_meta, $subscription ) {
		$payment_meta[ $this->id ] = array(
			'post_meta' => array(
				'_netbanx_payment_order_id' => array(
					'value' => get_post_meta( $subscription->id, '_netbanx_payment_order_id', true ),
					'label' => 'Netbanx Payment Transaction ID',
				),
			),
		);

		return $payment_meta;
	}

	/**
	 * Validate Payment method change
	 *
	 * @since 2.0
	 *
	 * @param $payment_method_id
	 * @param $payment_meta
	 *
	 * @throws Exception
	 */
	public function validate_subscription_payment_meta( $payment_method_id, $payment_meta ) {
		if ( $this->id === $payment_method_id ) {
			if ( ! isset( $payment_meta['post_meta']['_netbanx_payment_order_id']['value'] )
				|| empty( $payment_meta['post_meta']['_netbanx_payment_order_id']['value'] )
			) {
				throw new Exception( 'A "_netbanx_payment_order_id"(Netbanx Payment Transaction ID) value is required.' );
			}
		}
	}

	/**
	 * Charge the payment on order release
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 */
	public function process_pre_order_release_payment( \WC_Order $order ) {
		try {
			$request = new WC_Netbanx_Request_Hosted_Pre_Orders( $this );
			$rebill  = $request->process_pre_orders_rebill( $order );

			$response = new WC_Netbanx_Response_Hosted_Addons( $this );
			$response->process_order_based_on_response( $rebill, $order );
		}
		catch ( Exception $e ) {
			$order->add_order_note( $e->getMessage(), 'error' );
		}
	}

	/**
	 * Runs the process to obtain a payment URL from an order containing Pre-Orders.
	 *
	 * @since 2.0
	 *
	 * @param $order
	 *
	 * @return string
	 */
	private function get_pre_orders_payment_url( $order ) {
		$request = new WC_Netbanx_Request_Hosted_Pre_Orders( $this );
		$url     = $request->get_payment_url( $order );

		return $url;
	}

	/**
	 * Runs the process to obtain a payment URL from an order containing Subscriptions.
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	private function get_subscriptions_payment_url( WC_Order $order ) {
		$request = new WC_Netbanx_Request_Hosted_Subscriptions( $this );
		$url     = $request->get_payment_url( $order );

		return $url;
	}

	/**
	 * Returns true, if order contains Subscription
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return bool
	 */
	public function order_contains_subscription( WC_Order $order ) {
		if (
			2 === WC_Netbanx::get_subscriptions_version()
			&& (
				wcs_order_contains_subscription( $order )
				|| wcs_order_contains_renewal( $order )
			)
		) {
			return true;
		}

		return false;
	}

	/**
	 * Returns true, if order contains Pre-Order
	 *
	 * @since 2.0
	 *
	 * @param WC_Order $order
	 *
	 * @return bool
	 */
	public function order_contains_pre_order( WC_Order $order ) {
		if (
			WC_Netbanx::is_pre_orders_active()
			&& WC_Pre_Orders_Order::order_contains_pre_order( $order )
		) {
			return true;
		}

		return false;
	}
}