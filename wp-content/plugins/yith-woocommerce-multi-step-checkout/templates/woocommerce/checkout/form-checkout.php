<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$is_user_logged_in = is_user_logged_in();

$button_class = apply_filters( 'yith_wcms_step_button_class', 'button alt' );

$labels = apply_filters( 'yith_wcms_timeline_labels', array(
        'next'     => __( 'Continue', 'yith-woocommerce-multi-step-checkout' ),
        'prev'     => __( 'Previous', 'yith-woocommerce-multi-step-checkout' ),
        'login'    => _x( 'Login', 'Checkout: user timeline', 'yith-woocommerce-multi-step-checkout' ),
        'billing'  => _x( 'Billing', 'Checkout: user timeline', 'yith-woocommerce-multi-step-checkout' ),
        'shipping' => _x( 'Shipping', 'Checkout: user timeline', 'yith-woocommerce-multi-step-checkout' ),
        'order'    => _x( 'Order Info', 'Checkout: user timeline', 'yith-woocommerce-multi-step-checkout' ),
        'payment'  => _x( 'Payment Options', 'Checkout: user timeline', 'yith-woocommerce-multi-step-checkout' )
    )
);

$display = apply_filters( 'yith_wcms_timeline_display', 'horizontal' );

$timeline_args = array(
    'labels'            => $labels,
    'is_user_logged_in' => $is_user_logged_in,
    'display'           => $display,
    'style'             => get_option( 'yith_wcms_timeline_template', 'text' )
);

$timeline_template = apply_filters( 'yith_wcms_timeline_template', 'checkout-timeline.php' );

$enable_nav_button = 'yes' == get_option( 'yith_wcms_nav_buttons_enabled', 'yes' ) ? true : false;

//load timeline template
function_exists( 'yith_wcms_checkout_timeline' ) && yith_wcms_checkout_timeline( $timeline_template, $timeline_args );

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );
?>

<div id="checkout-wrapper" class="timeline-<?php echo $display ?>">
    <div id="checkout_coupon" class="woocommerce_checkout_coupon">
        <?php do_action( 'yith_woocommerce_checkout_coupon', $checkout ); ?>
    </div>

    <div id="checkout_login" class="woocommerce_checkout_login">
        <?php
        woocommerce_login_form(
            array(
                'message'  => __( 'If you have shopped with us before, please enter your details in the boxes below. If you are a new customer please proceed to the Billing &amp; Shipping section.', 'yith-woocommerce-multi-step-checkout' ),
                'redirect' => wc_get_page_permalink( 'checkout' ),
                'hidden'   => false
            )
        ); ?>
    </div>

    <?php

    // If checkout registration is disabled and not logged in, the user cannot checkout
    if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
        echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
        return;
    }

    // filter hook for include new pages inside the payment method
    $get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>

    <form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">

        <?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

            <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

            <div class="checkout_billing <?php echo $is_user_logged_in ? 'logged-in' : 'not-logged-in'; ?>" id="customer_billing_details">
                <?php do_action( 'woocommerce_checkout_billing' ); ?>
            </div>

            <div class="checkout_shipping" id="customer_shipping_details">
                <?php
                    do_action( 'woocommerce_before_checkout_shipping_form' );
                    do_action( 'woocommerce_checkout_shipping' );
                ?>
            </div>

            <?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
        <?php endif; ?>

        <?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

        <div id="order_review" class="woocommerce-checkout-review-order">
            <h3 id="order_review_heading"><?php _e( 'Order Info', 'woocommerce' ); ?></h3>
            <?php do_action( 'woocommerce_checkout_order_review' ); ?>
            <?php do_action( 'yith_woocommerce_checkout_order_review' ); ?>
            <input type="checkbox" name="payment_method" value="" data-order_button_text="" style="display: none;" />
        </div>

        <div id="order_checkout_payment" class="woocommerce-checkout-payment">
            <?php do_action( 'yith_woocommerce_checkout_payment' ); ?>
        </div>

        <?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

    </form>

    <div id="form_actions" class="<?php echo $enable_nav_button ? 'enabled' : 'disabled'; ?>" data-step="<?php echo $is_user_logged_in ? 1 : 0; ?>">
        <input type="button" class="<?php echo $button_class ?> prev" name="checkout_prev_step" value="<?php echo $labels['prev'] ?>" data-action="prev">
        <input type="button" class="<?php echo $button_class ?> alt next" name="checkout_next_step" value="<?php echo $labels['next'] ?>" data-action="next">
    </div>
</div>




<!-- CART SIDE VIEW -->
<div class="cart-side-view">
    <h3>Your Order</h3>
<?php
        foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
            $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

            if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                ?>
                <tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">


                    <div class="product-thumbnail">
                        <?php
                            $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                            if ( ! $_product->is_visible() ) {
                                echo $thumbnail;
                            } else {
                                printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
                            }
                        ?>
                    </div>

                    <div class="product-name" data-title="<?php _e( 'Product', 'woocommerce' ); ?>">
                        <?php
                            if ( ! $_product->is_visible() ) {
                                echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
                            } else {
                                echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
                            }

                            // Meta data
                            echo WC()->cart->get_item_data( $cart_item );

                            // Backorder notification
                            if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
                            }
                        ?>
                    </div>

                    <div class="product-price" data-title="<?php _e( 'Price', 'woocommerce' ); ?>">
                        <?php
                            echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                        ?>
                    </div>

                    <div class="product-quantity" data-title="<?php _e( 'Quantity', 'woocommerce' ); ?>">
                        <?php
                            if ( $_product->is_sold_individually() ) {
                                $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                            } else {
                                $product_quantity = woocommerce_quantity_input( array(
                                    'input_name'  => "cart[{$cart_item_key}][qty]",
                                    'input_value' => $cart_item['quantity'],
                                    'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                    'min_value'   => '0'
                                ), $_product, false );
                            }

                            echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
                        ?>
                    </div>

<!--                    <div class="product-subtotal" data-title="<//?php _e( 'Total', 'woocommerce' ); ?>">
                        <//?php
                            echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
                        ?>
                    </div> -->

                    <div class="product-remove">
                        <?php
                            echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                '<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
                                esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
                                __( 'Remove this item', 'woocommerce' ),
                                esc_attr( $product_id ),
                                esc_attr( $_product->get_sku() )
                            ), $cart_item_key );
                        ?>
                    </div>
                </tr>
                <?php
            }
        }

        do_action( 'woocommerce_cart_contents' );
        ?>
    <div class="help_banner">
        <img src="<?php echo get_template_directory_uri(); ?>/img/help_banner.png"/>
        <div class="help_banner_overlay">
            <h6>Have Questions? <br> We're here to Help</h6>
            <br>
            <p>Email us at <a href="mailto:info@hawaiiantropicapparel.ca">info@hawaiiantropicapparel.ca</a></p>
            <p>Expect to receive a reply within 48 hours.</p>
            <p><i>SHIPPING IN THE U.S. ONLY</i></p>
        </div>
        
    </div>
<?php do_action( 'woocommerce_after_checkout_form', $checkout );


