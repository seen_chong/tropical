<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tropical_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>v%b<_H<D=Soq(-zU.wb`+J)RP#fL*2z:^Eq[|MG34o>#f+x&!q)ow!v!4NE`Ess');
define('SECURE_AUTH_KEY',  '.=`f*}p#f*=t%nk4=kejg0n5FO]&W CO++,}~r_ 7Yxrci%J(Mn.tvALtO+YLx2#');
define('LOGGED_IN_KEY',    'mNqT<QP`s=-}labD-U-rAA-KP{m{L]p[ZDx1>-R<ExTV;~!vc*aL6M53Ko^]nkvC');
define('NONCE_KEY',        '&jB+$2Y8_j sq-`uu:~i6T{[LSmbNj;TRRu^AaO)R!.IC[&.0myF~V%zyM%]:<_@');
define('AUTH_SALT',        'o[V-9f%pE/Ipc!|?=BGqk^[%}q$?0Os-o*QU)hz*pnP h--,UwfAhy?y^j6:+2kk');
define('SECURE_AUTH_SALT', 'M5${OR|QlT{A):x-W)PN2N?}P)Ln(vq4ng(@sWjXD^}p(5]E[/bGj!xAv`)Y8Tf/');
define('LOGGED_IN_SALT',   'z2oFbt#QE!?*%rJL@%_mOMTvZ_7ST@58}K) Z~EO shgh<6-}ApSE(^`e7}HDy7H');
define('NONCE_SALT',       'K0`@seC+T5*iTeLG;f0endwP=C#+7jYI8 )fC7Ld<3%F@ZM>/vKo5smCGxfBxvX@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
