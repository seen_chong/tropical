<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tropical_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pN_,n#+DQyC4 =tk1Ucmtdxy]y(L;v.y*D2}KMcMz(Rg&%,-U:/U Hl8myJzf+6r');
define('SECURE_AUTH_KEY',  '(+4jxPRi}@8db_1x![A:6vrBCGuF=k93x|*V[e1R]7p*c}iLol--Y7hmMO8;0+-&');
define('LOGGED_IN_KEY',    '-)t8Dj/7wm=G-*r~{N(C@QK,!P-a~IL H5fl3>:u+n0:|`:ajmt;A8E?K+b-k2Zf');
define('NONCE_KEY',        'i[b<uX(Yy+Xi&=CxEH!du(y8g:!94ws=)3ga<j,6_?zeM=N+Q|beaA-0Ell61hFh');
define('AUTH_SALT',        ']p^LZCG=@#K1k:zL8pN:g#)O3nd3nIrS:]~6-:l?-Cjm0[ne_+B5X?M%1TStY4|=');
define('SECURE_AUTH_SALT', '|p%gBF;rH$_3.Siv&K k0!dWK2{&u:v-iNG1*63}+sftR[;8Y?4G>1r>;UVUyu>A');
define('LOGGED_IN_SALT',   'R)yko0b?$L_O38MslYJO_;G|gb0d4v@Sbh+aU^Rs9flT.B^*f1ooo+_Cpr#:+GSl');
define('NONCE_SALT',       ' r&}y>I$g8JQ_</v24~hQEI/So_B$_DI!9lbbL_dCiXn`QUB|N85:qGthFR@q8ei');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
